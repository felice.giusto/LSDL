package com.eng.lsdl.gateway;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.eng.lsdl.common.LSDLException;



public class LSDLUserProfile implements LSDLProfile {

  /*
  ** NOTA BENE: per abilitare l'output su System.out porre a true
  ** il flag enablePrint
  */
  
  /* Nome della classe */
  private static String className = "SDLUserProfile";

  private String response   = null;
  private String msisdn     = null;
  private String returnCode = null;
  private String networkLT  = null;


/*
** Costruttore
*/
public LSDLUserProfile (String serverResponse) throws LSDLException
{
  String methodName      = className + ".SDLUserProfile";
  Logger logger = Logger.getLogger (methodName);
  
  logger.debug("Begin.");
  response = serverResponse.trim();
  
  logger.debug("Parsing SDL response... ");
  parseResponse();
  logger.debug("End.");
}

public LSDLUserProfile (String theMsisdn, Character theLineType) throws LSDLException
{
	returnCode	=	(theLineType==null) ? "0001" : "0000";
	msisdn		=	theMsisdn;
	networkLT	=	(theLineType!=null) ? theLineType.toString() : null;
}

/* Ritorna l'intera stringa di risposta dal server */
public String getResponse()
{
  return response;
}

/* Ritorna la stringa contenente il numero dato in input */
public String getMsisdn()
{
  return msisdn;
}

/* Ritorna il codice di risposta del server 
** Il codice puo' essere:
** 0000 = Operazione OK (Analizzare anche il ritorno di getNetworkLineType)
** 0001 = Operazione OK (Numero non trovato nel DB)
** 0002 = Errore interno
** 0003 = Errore nella richiesta client
*/
public String getReturnCode () 
{
  return returnCode;
}

/* Ritorna il tipo di linea (O=OPSC,P=PPAS) di appartenenza 
** del numero (msisdn) selezionato 
*/
public String getNetworkLineType ()
{
  return networkLT;
}

private void parseResponse() throws LSDLException
{
  String methodName      = "className" + ".parseResponse";
  Logger logger = Logger.getLogger (methodName);

  logger.debug("Begin");

  try {
    /*
    * Implementare il codice per l'estrapolazione
    * dei dati richiesti: msisdn, returncode, lineType, ecc.
    * dalla stringa che ha il seguente formato di esempio:
    *
    ** "0000 3515909608,B"
    * "0001"
    * 
    */
	logger.info ("Input string is <" + response + ">");
    int counter = 0;
    StringTokenizer st = new StringTokenizer(response);
    while (st.hasMoreTokens()) {
      
      switch (counter) {
        
        /* Caso del codice di ritorno */
        case 0:
          returnCode = st.nextToken().trim();
          logger.debug ("ReturnCode is <" + returnCode + ">");
          break;
          
        /* Caso msisdn */
        case 1:
          msisdn = st.nextToken(",").trim();
          logger.debug ("Msisdn is <" + msisdn + ">");
          break;
          
        /* Caso NetworkLineType */
        case 2:
          networkLT  = st.nextToken().trim();
          logger.debug ("NetworkLT is <" + networkLT + ">");
          break;
          
        default:
        logger.warn ("Tokenizer error");
          throw new LSDLException ("Tokenizer error");
      }
      
      counter++;
    
    }
    
  }
  catch (LSDLException myUPEx)
  {
	logger.error("SDLException: "+myUPEx);
	throw myUPEx;
  }

  logger.debug("End");

} // End method
} // End class
