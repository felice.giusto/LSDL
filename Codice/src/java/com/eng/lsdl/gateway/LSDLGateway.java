package com.eng.lsdl.gateway;

import org.apache.log4j.Logger;

import com.eng.lsdl.common.ConfigurationManager;
import com.eng.lsdl.common.LSDLConstants;
import com.eng.lsdl.common.LSDLException;
import com.eng.lsdl.common.Parameters;
import com.eng.lsdl.core.LinesManager;

public class LSDLGateway implements LSDLConstants {

  private static ConfigurationManager configurator;

  private static LSDLGateway me = null;
  private static final String className = "SDLGateway";
	
	protected LSDLGateway () throws LSDLException 
	{
	  String methodName = className + ".SDLGateway";
	  Logger logger = Logger.getLogger(methodName);	  	  

	  try {
		  logger.info("Start");
		  configurator = ConfigurationManager.getConfigurator();  
	  }
	  catch (Exception cex) {
      logger.error ("Exception --> " + cex);
	    throw new LSDLException (Parameters.MSG_SDL_SYSTEM_GENERIC_ERROR);
	  }
	  logger.info("End");
	}

	public static LSDLGateway getInstance() throws LSDLException
	{
	  String methodName = className + ".SDLGateway";
	  Logger logger = Logger.getLogger (methodName);

	  if (me == null ) {
		synchronized (com.eng.lsdl.gateway.LSDLGateway.class ) {
		  if (me == null ) {
		    logger.info ("com.atosorigin.mcss.sdl.SDLGateway()");
		    me = new com.eng.lsdl.gateway.LSDLGateway();
		  } 
		  else {
	        logger.info ("SDLGateway Retrieved.");
	      }
	    }
	  }
	  return me;
	}
	
	public Character getNetworkLineType (String msisdn) throws LSDLException {
		String noCache = configurator.getParameterValue(Parameters.PARAM_LSDL_NO_CACHE);
		return getNetworkLineType (msisdn, noCache);
	}
	
	public Character getNetworkLineType (String msisdn, String noCache) throws LSDLException {
		String methodName = className + ".getLSDLNetworkLineType";
				  
		LinesManager theLineManager			= null;
		Character theLineMarket				= null;
		  	
		Logger logger = Logger.getLogger (methodName);
		logger.info ("<"+msisdn+"> Start");
		logger.info ("Input msisdn is <" + msisdn + ">");
		logger.debug("Lines File <" + configurator.getParameterValue(Parameters.PARAM_LSDL_LINES_FILE) + ">");
		logger.debug("Cache name <" + configurator.getParameterValue(Parameters.PARAM_LSDL_CACHE_NAME) + ">");
		logger.debug("Msisdn Filter <" + configurator.getParameterValue(Parameters.PARAM_LSDL_MSISDN_FILTER) + ">");
		
		logger.debug("Reading from cache <" + noCache + ">");
	
		
		
		logger.debug("<"+msisdn+">"+"Getting Line Manager");
		theLineManager = LinesManager.getLinesManager(configurator.getParameterValue(Parameters.PARAM_LSDL_LINES_FILE),configurator.getParameterValue(Parameters.PARAM_LSDL_CACHE_NAME),configurator.getParameterValue(Parameters.PARAM_LSDL_MSISDN_FILTER));
		
		
		logger.debug("Searching msisdn <" + msisdn + "> in LSDL");
		theLineMarket = theLineManager.searchLine(msisdn, noCache);
		logger.debug("Msisdn <"+msisdn+"> has market <"+theLineMarket+">");
		  		 	    
//		theSdlUprofile = new LSDLUserProfile(msisdn,theLineMarket);	
//	
//		 /* Building response object */
//		  try {
//			  logger.debug ("Building response Helper Object()");
//			  theSdlUHelper = new LSDLUserProfileHelper (theSdlUprofile);
//		  }
//		  catch (Exception ex) {
//			  logger.error ("msisdn <"+msisdn+"> bad operation: " + ex.toString());
//			  throw new LSDLException (Parameters.MSG_SDL_SYSTEM_GENERIC_ERROR);
//		  }
	
		  logger.info ("<"+msisdn+"> End");
	
		  return theLineMarket; 	  
	}
}
