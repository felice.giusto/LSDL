package com.eng.lsdl.gateway;

public interface LSDLProfile 
{
  
  public String getMsisdn          ();
  public String getReturnCode      ();
  public String getNetworkLineType ();  
}
