package com.eng.lsdl.common;


import org.apache.log4j.Logger;

import com.eng.lsdl.gateway.LSDLUserProfile;

public class LSDLUserProfileHelper implements LSDLConstants {
	
	private String msisdn     = null;
	private String returnCode = null;
    private String lineType  = MCSS_PREPAGO;
        
    private static final String className = "SDLUserProfileHelper";

public LSDLUserProfileHelper (LSDLUserProfile theUserProf) throws LSDLException 
{
	String methodName = className + ".SDLUserProfileHelper";
	Logger logger = Logger.getLogger (methodName);
	ConfigurationManager configurator = null;
	
	try {	 
		logger.info ("Start");
		
		configurator = ConfigurationManager.getConfigurator(); 
		
	    msisdn     = theUserProf.getMsisdn();
	    returnCode = theUserProf.getReturnCode ();
	    
//	    String tmpLineType = configurator.getParameterValue(Parameters.PARAM_SDL_LINETYPE+theUserProf.getNetworkLineType());
//	    
//	    if(tmpLineType.equalsIgnoreCase(MCSS_POSTPAGO))
//	    	lineType = MCSS_POSTPAGO;
	    
	    lineType = configurator.getParameterValue(Parameters.PARAM_SDL_LINETYPE+theUserProf.getNetworkLineType());
	    
	} catch (Exception gEx) {
	    logger.error("Exception in SDLUserProfileHelper constructor: " + gEx);
	    throw new LSDLException(gEx.getMessage());
	}
}

  // Metodi get
  public String getMsisdn ()
  {
    return msisdn;
  }
  
  public String getReturnCode ()
  {
	return returnCode;
  }

  public String getLineType ()
  {
	return lineType;
  }

}
