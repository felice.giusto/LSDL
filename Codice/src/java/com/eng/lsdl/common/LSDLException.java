package com.eng.lsdl.common;

public class LSDLException extends Exception{
	  public LSDLException(String s){
	    super(s);
	  }
	  public LSDLException(){
	    super();
	  }
	}
