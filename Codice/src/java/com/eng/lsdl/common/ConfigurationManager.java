package com.eng.lsdl.common;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.eng.lsdl.common.Parameters;


/**
 * System Configuratr.
 *
 * Implements pattern Singleton.
 *
 * Provides factory methods to initialize the application parameters
 * inclusive one necessary to LOG4J objects request
 *
 * @author Roberto Nevola
 * @version 1.0
 * @stereotype singleton
 * @see <{Parameters}>
 * @testcase test.com.atosorigin.mcss.common.TestConfigurationManager
 */
public class ConfigurationManager implements Serializable {

  private static final long serialVersionUID = 4288494434446339687L;
  private static Logger logger = null;
  private static ConfigurationManager instance = null;
  private static Parameters parameters = null;


  private Hashtable hashConfig = new Hashtable();
  private static final String className = "ConfigurationManager";
  private Properties defaultLog4jProps;
  private Properties bulkLog4jProps = new Properties();;

  /**
   * @link
   * @shapeType PatternLink
   * @pattern Singleton
   * @supplierRole Singleton factory
   */
  /*# private ConfigurationManager _configurationManager; */

  /**
   * Constructor
   * Loads System configuration from Database
   * via Parameters class creation
   *
   */
  protected ConfigurationManager() {
    System.out.println("ConfigurationManager - Begin");
    parameters = new Parameters();
    System.out.println("ConfigurationManager - Parameters OK.");
    //loadLog4jDefaultParameters();

    //logger = Logger.getLogger(parameters.getValue(Parameters.PARAM_LOG4J_MCSS_GROUP));
    String methodName = className + ".ConfigurationManager";
    Logger logger = Logger.getLogger(methodName);

    loadConfig();

    System.out.println("Configuring LOG4J...");
    getLog4jLogger();
    System.out.println("ConfigurationManager - Getting Logger ...");

    logger.info("ConfigurationManager Client built.");
  }

  /**
   * Factory method.
   */
  public static ConfigurationManager getConfigurator() {
    if (instance == null) {
      //System.out.println("ConfigurationManager - getConfigurator 1");
      synchronized (com.eng.lsdl.common.ConfigurationManager.class) {
        if (instance == null) {
          //System.out.println("ConfigurationManager - getConfigurator 2");
          instance = new com.eng.lsdl.common.ConfigurationManager();
          //System.out.println("ConfigurationManager - getConfigurator 3");
        }
      }
    }
    return instance;
  }
  
  /**
   * Force Istance reload to refresh Configuration
   */
  public static synchronized ConfigurationManager refreshConfigurator() {
          instance = new com.eng.lsdl.common.ConfigurationManager();
          return instance;
  }

  /**
   * Restituisce l'oggetto Log4j configurato.
   * Legge dai parametri dell'applicazione quelli relativi al log4j.
   * Esegue un log(INFO) per segnalare la corretta attivazione del log.
   */
  private void getLog4jLogger() {
    String methodName = className + ".getLog4jLogger";
    Logger logger = Logger.getLogger(methodName);
    Properties log4jProps = new Properties();
    Enumeration theEnum = parameters.getKeys();
    String param;
    String value;
    while (theEnum.hasMoreElements()) {
      param = (String) theEnum.nextElement();
      if (param.startsWith("log4j")) {
        value = (String) parameters.getValue(param);
        log4jProps.setProperty(param, value);
      }
    }

    PropertyConfigurator.configure(log4jProps);
    logger.info("LOG4J configured.");
  }

  /**
   * Utilizza i loggroup alla maniera 2.0
   * Usata da bulk.
   * Restituisce l'oggetto Log4j configurato.
   * Legge dai parametri dell'applicazione quelli relativi al log4j.
   * Esegue un log(INFO) per segnalare la corretta attivazione del log.
   */
  public Logger getLog4jLogger(String logName) {
    Properties log4jProps = new Properties();
    Enumeration theEnum = parameters.getKeys();
    String param;
    String value;
    while (theEnum.hasMoreElements()) {
      param = (String) theEnum.nextElement();
      if (param.startsWith("log4j")) {
        value = (String) parameters.getValue(param);
        log4jProps.setProperty(param, value);
      }
    }

    if (logName == null || logName.equals("")) {
      logName = parameters.getValue(Parameters.PARAM_LOG4J_MCSS_GROUP);
    }
    Logger newLogger = Logger.getLogger(logName);

    PropertyConfigurator.configure(log4jProps);

    newLogger.info("LOG4J <" + logName + "> configured.");

    return newLogger;
  }
  
  /**
   * Crea il log di report per ciascun CSP.
   * Restituisce l'oggetto Log4j configurato.
   * Legge dai parametri dell'applicazione quelli relativi al log4j.
   * Esegue un log(INFO) per segnalare la corretta attivazione del log.
   */
  public Logger getLog4jBulkOutLogger(String theCSP) {
	String methodName = className + ".getLog4jBulkOutLogger";
	Logger logger = Logger.getLogger(methodName);  
	  
	logger.debug("Begin.");	
       
    String theBulkLogAdditivity		= this.getParameterValue(Parameters.PARAM_LOG4J_ADDITIVITY_BULKLOGGER);
    String theBulkLogFile			= this.getParameterValue(Parameters.PARAM_LOG4J_APPENDER_BULKROLLINGFILE);
    String theBulkLogLayout			= this.getParameterValue(Parameters.PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUT);
    String theBulkbulkLogPattern	= this.getParameterValue(Parameters.PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUTPATTERN);
    String theBulkLogger			= this.getParameterValue(Parameters.PARAM_LOG4J_BULKLOGGER);
    String theBulkLogFileName 		= this.getParameterValue(Parameters.PARAM_LOG4J_APPENDER_BULKROLLINGFILE_NAME);
        
    if (bulkLog4jProps.getProperty("log4j.logger."+theCSP)==null) {
    	bulkLog4jProps.setProperty("log4j.logger."+theCSP,theBulkLogger+theCSP+"_");
    	bulkLog4jProps.setProperty("log4j.additivity."+theCSP,theBulkLogAdditivity);    	
    	bulkLog4jProps.setProperty("log4j.appender."+theCSP+"_.layout",theBulkLogLayout);
    	bulkLog4jProps.setProperty("log4j.appender."+theCSP+"_.layout.ConversionPattern",theBulkbulkLogPattern);
    	bulkLog4jProps.setProperty("log4j.appender."+theCSP+"_.File",theBulkLogFileName+theCSP+".log");
    	bulkLog4jProps.setProperty("log4j.appender."+theCSP+"_",theBulkLogFile);
    	
    	logger.debug("Log4jProperties: ["+bulkLog4jProps.toString()+"]");
    	
    	PropertyConfigurator.configure(bulkLog4jProps);   	
    } else
    	logger.debug("Bulk Log for Csp ["+theCSP+"] already Cunfigured.");
    
    Logger newLogger = Logger.getLogger(theCSP);   

    logger.debug("End.");
    return newLogger;
  }
  
  /**
   * Clona il Bulk Report LOG di MCSS modificando File Name e livelllo di LOG.
   * ES: Usata dall'SDL Gateway per tracciare gli MSISDN errati. 
   * Restituisce l'oggetto Log4j configurato.
   * Legge dai parametri dell'applicazione quelli relativi al log4j.
   * Esegue un log(INFO) per segnalare la corretta attivazione del log.
   */
  public Logger cloneLog4jLogger(String theLogId, String theFileName) {
	String methodName = className + ".cloneLog4jLogger";
	Logger logger = Logger.getLogger(methodName); 
	Logger newLogger = null;
	  
	logger.debug("Begin.");	                 
	newLogger = Logger.getLogger(theLogId);    
    logger.debug("End.");
    return newLogger;
  }
  
  /**
   * Returns a Vector containing a set of properties
   * having the given prefix
   */
  public Vector getProgertiesGroup(String havingPrefix) {
    Vector thePropsGroup = new Vector();
    Enumeration theEnum = parameters.getKeys();
    String param;
    String value;
    while (theEnum.hasMoreElements()) {
      param = (String) theEnum.nextElement();
      if (param.startsWith(havingPrefix)) {
        value = (String) parameters.getValue(param);
        thePropsGroup.add(value);
      }
    }

    return thePropsGroup;
  }

  /**
   * Returns a Vector containing all SDL error codes not subject to retry.
   */
  public Vector<String> getSDLErrors() {
    Vector<String> result = new Vector<String>();
    
    String value = this.getParameterValue(Parameters.PARAM_SDL_ERROR_CODES);
    String elem = null;

    StringTokenizer tokens = new StringTokenizer(value, ",");

    while (tokens.hasMoreElements()) {
      elem = (String) tokens.nextElement();
      result.add(elem);
    }

    return result;
  }

 public Parameters getParameters() {
    return parameters;
  }

  /**
   * (pentimento dell'ultima ora! (rnevola))
   * Metodo per la lettura del valore di un parametro conoscendo la chiave.
   */
  public String getParameterValue(String key) {
    return parameters.getValue(key);
  }

  /**
   * (esperienza del pentimento dell'ultima ora! (rnevola))
   * Metodo per la lettura del valore boolean di un parametro conoscendo la chiave.
   */
  public boolean getBooleanParameterValue(String key) {
    String value = (String) parameters.getValue(key);
    boolean ret = false;

    try {
      // controlla se il parametro e': Yes or True or Si...
      ret = (value.toUpperCase().startsWith("Y") ||
             value.toUpperCase().startsWith("T") ||
             value.toUpperCase().startsWith("S"));
    }
    catch (Exception ex) {
      logger.warn ("Exception while reading boolean param <" + key + "> value <" + value + ">: " + ex);
    }

    return ret;
  }

  /**
   * Restituisce il msg associato alla costante key.
   */
  public String getMessage(String key) {
    return parameters.getMessage(key);
  }

  /**
   * Restituisce il Type msg associato alla costante key.
   */
  public String getTypeMessage(String key) {
    return parameters.getTypeMessage(key);
  }

  /**
   * Restituisce il codice del msg associato alla costante key.
   */
  public String getMessageCode(String key) {
    return parameters.getMessageCode(key);
  }

  private void loadConfig() {
    String methodName = className + ".loadConfig";
    Logger logger = Logger.getLogger(methodName);

    logger.debug("BEGIN");
    String tmpValue = null;
    Object tmpKey = null;
    Hashtable tmpConfigHash = null;
    Properties configurationProperties = new Properties();
    
    logger.debug("Loading configuration...");
    try {
      String configurationFilePathName = System.getProperty(Parameters.PARAM_CONFIG_FILE_PATH_NAME, "");
      InputStream propertiesStream = new FileInputStream(configurationFilePathName);
     
      configurationProperties.load(propertiesStream);
          
      Map<String,String> propertiesMap = new HashMap(configurationProperties);
      tmpConfigHash = new Hashtable(propertiesMap);
    }
    catch (Exception gEx) {
      logger.error("Unable to load configuration File");
    }

    for (Enumeration configKeys = tmpConfigHash.keys();
         configKeys.hasMoreElements(); ) {
      logger.debug("Read parameter...");
      tmpKey = configKeys.nextElement();
      tmpValue = (String) tmpConfigHash.get(tmpKey);
      logger.debug("Read parameter <" + tmpKey + "> value <" + tmpValue + ">");
      parameters.setParameter( (String) tmpKey, tmpValue);
      logger.debug("Added parameters.");
    }

    logger.debug("End reading configuration...OK");

  }
}
