package com.eng.lsdl.common;

public interface LSDLConstants {
	public final static String MCSS_MSISDN_REP	= "MSISDNREP";
	
	public final static String MCSS_POSTPAGO	= "POSTPAGO";
	public final static String MCSS_PREPAGO		= "PREPAGO";

	public final static String REPLICATED_LSDL_CACHE	= "REPL-LSDL-PrepaidCache";
	
	public final static char MCSS_YES	= 'Y';
	public final static char MCSS_NO	= 'N';
		
	public final static String MCSS_TRUE	= "true";
	public final static String MCSS_FALSE	= "false";
	
	public final static String MCSS_ENABLED  = "E";
	public final static String MCSS_DISABLED = "D";
	
    /* The following is used for the validity period of an authorization codes
     *	which should never expires
     */
    public final static long  FOREVER=-1L;

    /* The following is used to force code expiration    */
    public final static long  DOITEXPIRE=-2L;    
}