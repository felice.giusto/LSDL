package com.eng.lsdl.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.PropertyConfigurator;

/**
 * @author Luca D'Agostino
 * @version 2.0
 */
public class Parameters implements Serializable {
    Hashtable params;
    Properties L4JDef;
    private static final String className = "Parameters";
    private ArrayList mandatoryList;
    // Total Nodes in the Sysstem

	// WLS Credential
	public final static String PARAM_USE_CREDENTIAL = "mcss.use.credentials";
    public final static String PARAM_CREDENTIAL_USR = "mcss.credentials.usr";
    public final static String PARAM_CREDENTIAL_PWD = "mcss.credentials.pwd";
    
    //ACTYION ON OPSC FAILURE
    public final static String PARAM_STOP_ON_OPSC_FAILURE = "param.stop.on.opsc.failure";
    public final static String PARAM_STOP_ON_PFE_ERRORS	  = "param.stop.on.pfe.error.codes";    

	// public final static String PARAM_CONFIGURATION_NODES =
	// "mcss.configuration.nodes";
    // Node ID Value
    public final static String PARAM_CONFIGURATION_NODE_ID = "mcss.configuration.node.id";
    // Config Table Name
    public final static String PARAM_CONFIGURATION_TABLE_NAME = "mcss.configuration.table.name";

    /** parametri di sistema... */
    public final static String PARAM_MSISDN_FORMAT_EXPRESSION = "mcss.msisdn.format.expression";
    public final static String PARAM_CONFIGURATION_REFRESH_TIME_MINUTES = "mcss.configuration.refresh.time.minutes";
    public final static String PARAM_USE_DAYLIGHT_SAVING_TIME = "mcss.use.daylight.saving.time";
    public final static String PARAM_CURRENCY_FACTOR_MULTIPLIER_FOR_OPSC = "mcss.currency.factor.multiplier.for.opsc";
    public final static String PARAM_BALANCE_USE_TOC_BASKET = "mcss.balance.use.toc.basket";
    

    /** nome dei parametri rionosciuti nella richiesta HTTP */    
    public final static String PARAM_STOP_ON_MISMATCH = "mcss.stop.on.mismatch";
    public final static String PARAM_HTTP_FUNCTION = "mcss.http.param.function";
    public final static String PARAM_HTTP_AUTHORIZATIONCODE = "mcss.http.param.authorization.code";
    public final static String PARAM_HTTP_PINCODE = "mcss.http.param.pin.code";
    public final static String PARAM_HTTP_UVI = "mcss.http.param.uvi";
    public final static String PARAM_HTTP_MSISDN = "mcss.http.param.msisdn";
    public final static String PARAM_HTTP_TOTRECIP = "mcss.http.param.totalrecipient";
    public final static String PARAM_HTTP_RECIPIENT = "mcss.http.param.recipient";
    public final static String PARAM_HTTP_SERVICEID1 = "mcss.http.param.serviceid.1";
    public final static String PARAM_HTTP_SERVICEID2 = "mcss.http.param.serviceid.2";
    public final static String PARAM_HTTP_SERVICEID3 = "mcss.http.param.serviceid.3";
    public final static String PARAM_HTTP_CSP = "mcss.http.param.csp";
    public final static String PARAM_HTTP_USR = "mcss.http.param.user";
    public final static String PARAM_HTTP_PWD = "mcss.http.param.password";
    public final static String PARAM_HTTP_VOLUME = "mcss.http.param.volume";
    public final static String PARAM_HTTP_TIME = "mcss.http.param.time";
    public final static String PARAM_HTTP_CONTENT = "mcss.http.param.content";
    public final static String PARAM_HTTP_QUANTITY = "mcss.http.param.quantity";
    public final static String PARAM_HTTP_STARTTIME = "mcss.http.param.starttime";
    public final static String PARAM_HTTP_ENDTIME = "mcss.http.param.endtime";
    public final static String PARAM_HTTP_SERVICE_NAME = "mcss.http.param.service.name";
    public final static String PARAM_HTTP_TID = "mcss.http.param.tid";
    public final static String PARAM_HTTP_CREDRES = "mcss.http.credit.res";
    public final static String PARAM_HTTP_SERVICEKEY = "mcss.http.param.service.key";
    public final static String PARAM_HTTP_SEND_SMS = "mcss.http.param.send.sms";
    public final static String PARAM_HTTP_TEXT_SMS = "mcss.http.param.text.sms";
    public final static String PARAM_HTTP_OADC_SMS = "mcss.http.param.oadc.sms";

    public final static String PARAM_HTTP_TEXT_UNICODE_SMS = "mcss.http.text.unicode.sms";
	public final static String PARAM_HTTP_TEXT_UNICODE_BULK = "mcss.http.text.unicode.bulk";
	
	public final static String PARAM_HTTP_IS_REBILLING = "mcss.http.param.is.rebilling";
	public final static String PARAM_HTTP_ITEM_ID = "mcss.http.param.item.id";
	
	public final static String PARAM_OLM_URL = "mcss.olm.url";
	public final static String PARAM_OLM_DESTINATION_REALM = "mcss.olm.destination.realm";
	public final static String PARAM_OLM_MCSS_URI = "mcss.olm.mcss.uri";
	public final static String PARAM_OLM_MCSS_REALM = "mcss.olm.mcss.realm";
	public final static String PARAM_OLM_CONTEXT_ID = "mcss.olm.context.id";
	public final static String PARAM_OLM_HOST_IP = "mcss.olm.host.ip";

    /** valore del parametro function della richiesta HTTP */
    public final static String PARAM_HTTP_FUNCTION_AUTHORIZE   = "mcss.http.param.function.value.authorize";
    public final static String PARAM_HTTP_FUNCTION_BILLING	   = "mcss.http.param.function.value.billing";
    public final static String PARAM_HTTP_FUNCTION_CHECKTID    = "mcss.http.param.function.value.checktid";
    public final static String PARAM_HTTP_FUNCTION_RESROLLBACK = "mcss.http.param.function.value.resrollback";
    public final static String PARAM_HTTP_FUNCTION_RELOAD 	   = "mcss.http.param.function.value.reload";
    public final static String PARAM_HTTP_FUNCTION_CHECKUSER   = "mcss.http.param.function.value.checkuser";
    public final static String PARAM_HTTP_FUNCTION_RECYCLE 	   = "mcss.http.param.function.value.recycle";
    public final static String PARAM_HTTP_FUNCTION_RECYCLESMSO = "mcss.http.param.function.value.recycle.smso";
    public final static String PARAM_HTTP_FUNCTION_CHKANDBILL  = "mcss.http.param.function.value.chkandbill";
    public final static String PARAM_HTTP_FUNCTION_BULKMONITOR = "mcss.http.param.function.value.bulkmonitor";
    public final static String PARAM_HTTP_FUNCTION_PINGEN	   = "mcss.http.param.function.value.pingen";
    public final static String PARAM_HTTP_FUNCTION_PINBURN	   = "mcss.http.param.function.value.pinburn";

    /** nome dei parametri di configurazione */
    public final static String PARAM_ERRORPAGE = "mcss.error.page.generic";
    public final static String PARAM_ERRORPAGE_FOR_REQUEST = "mcss.error.page.request";
    public final static String PARAM_ERRORPAGE_FOR_COMMAND = "mcss.error.page.command";
    public final static String PARAM_ERRORPAGE_FOR_AUTHORIZE = "mcss.error.page.authorize";
    public final static String PARAM_ERRORPAGE_FOR_BILLING = "mcss.error.page.billing";
    public final static String PARAM_ERRORPAGE_FOR_CHECKTID = "mcss.error.page.checktid";
    public final static String PARAM_ERRORPAGE_FOR_RESROLLBACK = "mcss.error.page.resrollback";
    public final static String PARAM_ERRORPAGE_FOR_RECYCLE = "mcss.error.page.recycle";
    public final static String PARAM_ERRORPAGE_FOR_RECYCLESMSO = "mcss.error.page.recycle.smso";
    public final static String PARAM_ERRORPAGE_FOR_PINGEN				= "mcss.error.page.pingen";
    public final static String PARAM_ERRORPAGE_FOR_PINBURN				= "mcss.error.page.pinburn";
    public final static String PARAM_RESPONSEPAGE_FOR_MCSS = "mcss.response.page.mcss";
    public final static String PARAM_RESPONSEPAGE_FOR_AUTHORIZE = "mcss.response.page.authorize";
    public final static String PARAM_RESPONSEPAGE_FOR_BILLING = "mcss.response.page.billing";
    public final static String PARAM_RESPONSEPAGE_FOR_CHECKTID = "mcss.response.page.checktid";
    public final static String PARAM_RESPONSEPAGE_FOR_RESROLLBACK = "mcss.response.page.resrollback";
    public final static String PARAM_RESPONSEPAGE_FOR_RECYCLE   = "mcss.response.page.recycle";
    public final static String PARAM_RESPONSEPAGE_FOR_RECYCLESMSO   = "mcss.response.page.recycle.smso";
    public final static String PARAM_RESPONSEPAGE_FOR_PINGEN			= "mcss.response.page.pingen";
    public final static String PARAM_RESPONSEPAGE_FOR_PINBURN			= "mcss.response.page.pinburn";
    public final static String PARAM_RESPONSEPAGE_FOR_MONITOR_BULK   = "mcss.response.page.monitor.bulk";
    public final static String PARAM_INITIAL_CONTEXT_FACTORY = "mcss.naming.factory.initial";
    public final static String PARAM_INITIAL_CONTEXT_PROVIDER_URL = "mcss.naming.provider.url";
    public final static String PARAM_CONFIG_FILE_PATH_NAME = "lsdl.config.file.path.name";
    public final static String PARAM_EJB_CONFIGURATIONMANAGER_URL = "mcss.ejb.configuration.manager.url";
    public final static String PARAM_EJB_CONFIGURATIONMANAGER_JNDI = "mcss.ejb.configuration.manager.jndi";
    public final static String PARAM_EJB_CONFIGURATIONMANAGER_HOME = "mcss.ejb.configuration.manager.home";
    public final static String PARAM_EJB_AUTHORIZATIONSERVICE_URL = "mcss.ejb.authorization.service.url";
    public final static String PARAM_EJB_AUTHORIZATIONSERVICE_JNDI = "mcss.ejb.authorization.service.jndi";
    public final static String PARAM_EJB_AUTHORIZATIONSERVICE_HOME = "mcss.ejb.authorization.service.home";
    public final static String PARAM_EJB_BILLINGSERVICE_URL = "mcss.ejb.billing.service.url";
    public final static String PARAM_EJB_BILLINGSERVICE_JNDI = "mcss.ejb.billing.service.jndi";
    public final static String PARAM_EJB_BILLINGSERVICE_HOME = "mcss.ejb.billing.service.home";
    public final static String PARAM_EJB_CHECKTIDSERVICE_URL = "mcss.ejb.checktid.service.url";
    public final static String PARAM_EJB_CHECKTIDSERVICE_JNDI = "mcss.ejb.checktid.service.jndi";
    public final static String PARAM_EJB_CHECKTIDSERVICE_HOME = "mcss.ejb.checktid.service.home";
    public final static String PARAM_EJB_AUTHORIZATION_URL = "mcss.ejb.authorization.url.list";
    public final static String PARAM_EJB_AUTHORIZATION_JNDI = "mcss.ejb.authorization.jndi";
    public final static String PARAM_EJB_AUTHORIZATION_HOME = "mcss.ejb.authorization.home";
    public final static String PARAM_EJB_RESROLLBACK_URL = "mcss.ejb.resrollback.url";
    public final static String PARAM_EJB_RESROLLBACK_JNDI = "mcss.ejb.resrollback.jndi";
    public final static String PARAM_EJB_RESROLLBACK_HOME = "mcss.ejb.resrollback.home";

    public final static String PARAM_EJB_SOCKETWRAPPER_URL = "mcss.ejb.socketwrapper.url.";
    public final static String PARAM_EJB_SOCKETWRAPPER_JNDI = "mcss.ejb.socketwrapper.jndi.";
    public final static String PARAM_EJB_SOCKETWRAPPER_HOME = "mcss.ejb.socketwrapper.home.";
    
    public final static String PARAM_EJB_FILEWRITER_URL 	= "mcss.ejb.filewriter.service.url";
    public final static String PARAM_EJB_FILEWRITER_JNDI	= "mcss.ejb.filewriter.service.jndi";
    public final static String PARAM_EJB_FILEWRITER_HOME 	= "mcss.ejb.filewriter.service.home";
    
    public final static String PARAM_EJB_BULKBILLINGSERVICE_URL = "mcss.ejb.bulk.billing.service.url";
    public final static String PARAM_EJB_BULKBILLINGSERVICE_JNDI = "mcss.ejb.bulk.billing.service.jndi";
    public final static String PARAM_EJB_BULKBILLINGSERVICE_HOME = "mcss.ejb.bulk.billing.service.home";
    
    /** ICARD/MCSS Integration EJB: ICARD Credit ReservationServices */
    public final static String PARAM_EJB_ICARD_CREDRES_URL = "mcss.icard.ejb.credres.url.list";
    public final static String PARAM_EJB_ICARD_CREDRES_JNDI = "mcss.icard.ejb.credres.jndi";
    public final static String PARAM_EJB_ICARD_CREDRES_HOME = "mcss.icard.ejb.credres.home";
    
    public final static String PARAM_EJB_CHKANDBILLSERVICE_URL = "mcss.ejb.chkandbill.service.url";
    public final static String PARAM_EJB_CHKANDBILLSERVICE_JNDI = "mcss.ejb.chkandbill.service.jndi";
    public final static String PARAM_EJB_CHKANDBILLSERVICE_HOME = "mcss.ejb.chkandbill.service.home";
    
    public final static String PARAM_EJB_PINGEN_URL =  "mcss.ejb.pingen.service.url";
    public final static String PARAM_EJB_PINGEN_JNDI = "mcss.ejb.pingen.service.jndi";
    public final static String PARAM_EJB_PINGEN_HOME = "mcss.ejb.pingen.service.home";
    
    public final static String PARAM_EJB_PINBURN_URL =  "mcss.ejb.pinburn.service.url";
    public final static String PARAM_EJB_PINBURN_JNDI = "mcss.ejb.pinburn.service.jndi";
    public final static String PARAM_EJB_PINBURN_HOME = "mcss.ejb.pinburn.service.home";
       
    public final static String PARAM_ICARD_DEFAULT_ZERO_COST_SERVICE_CLASS	= "mcss.icard.default.zero.cost.service.class";
    public final static String PARAM_ICARD_DEFAULT_ZERO_COST_SERVICE_ID		= "mcss.icard.default.zero.cost.service.id";
    public final static String PARAM_ICARD_DEFAULT_ZERO_COST_EVENT_ID		= "mcss.icard.default.zero.cost.event.id";
    
    public final static String PARAM_ICARD_WALLET_TYPE			= "mcss.icard.wallet.type";
    public final static String PARAM_ICARD_HAPPY_HOUR_TYPE		= "mcss.icard.happy.hour.type";
    
    public final static String PARAM_ICARD_IS_USED		= "mcss.icard.is.used";
    

    // param Log4J
    public final static String PARAM_LOG4J_ROOTLOGGER = "log4j.rootLogger";
    public final static String PARAM_LOG4J_APPENDER_CONSOLE = "log4j.appender.stdout";
    public final static String PARAM_LOG4J_APPENDER_CONSOLE_LAYOUT = "log4j.appender.stdout.layout";
    public final static String PARAM_LOG4J_APPENDER_CONSOLE_LAYOUTPATTERN = "log4j.appender.stdout.layout.ConversionPattern";
    public final static String PARAM_LOG4J_APPENDER_FILE = "log4j.appender.stdfile";
    public final static String PARAM_LOG4J_APPENDER_FILE_NAME = "log4j.appender.stdfile.File";
    public final static String PARAM_LOG4J_APPENDER_FILE_LAYOUT = "log4j.appender.stdfile.layout";
    public final static String PARAM_LOG4J_APPENDER_FILE_LAYOUTPATTERN = "log4j.appender.stdfile.layout.ConversionPattern";
    public final static String PARAM_LOG4J_APPENDER_ROLLINGFILE = "log4j.appender.rollingfile";
    public final static String PARAM_LOG4J_APPENDER_ROLLINGFILE_NAME = "log4j.appender.rollingfile.File";
    public final static String PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUT = "log4j.appender.rollingfile.layout";
    public final static String PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUTPATTERN = "log4j.appender.rollingfile.layout.ConversionPattern";
    public final static String PARAM_LOG4J_BULKLOGGER = "log4j.logger.bulkLogger";
    public final static String PARAM_LOG4J_ADDITIVITY_BULKLOGGER = "log4j.additivity.bulkLogger";
    public final static String PARAM_LOG4J_APPENDER_BULKFILE = "log4j.appender.bulkfile";
    public final static String PARAM_LOG4J_APPENDER_BULKFILE_NAME = "log4j.appender.bulkfile.File";
    public final static String PARAM_LOG4J_APPENDER_BULKFILE_LAYOUT = "log4j.appender.bulkfile.layout";
    public final static String PARAM_LOG4J_APPENDER_BULKFILE_LAYOUTPATTERN = "log4j.appender.bulkfile.layout.ConversionPattern";
    public final static String PARAM_LOG4J_APPENDER_BULKROLLINGFILE = "log4j.appender.bulkrollingfile";
    public final static String PARAM_LOG4J_APPENDER_BULKROLLINGFILE_NAME = "log4j.appender.bulkrollingfile.File";
    public final static String PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUT = "log4j.appender.bulkrollingfile.layout";
    public final static String PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUTPATTERN = "log4j.appender.bulkrollingfile.layout.ConversionPattern";
    public final static String PARAM_LOG4J_MCSS_GROUP = "mcss.log4j.common.group";
    public final static String PARAM_LOG4J_CONTROLLER_GROUP = "mcss.log4j.controller.group";
    public final static String PARAM_LOG4J_CATALOG_GROUP = "mcss.log4j.catalog.group";
    public final static String PARAM_LOG4J_AUTHORIZE_GROUP = "mcss.log4j.authorize.group";
    public final static String PARAM_LOG4J_BILLING_GROUP = "mcss.log4j.billing.group";
    public final static String PARAM_LOG4J_OPSC_GROUP = "mcss.log4j.opsc.group";
    public final static String PARAM_LOG4J_BULK_GROUP = "mcss.log4j.bulk.group";
    public final static String PARAM_LOG4J_CHECKTID_GROUP = "mcss.log4j.checktid.group";

    /** nome dei parametri necessari al gateway PFE */
    public static final String PARAM_PFE_VERS              = "mcss.pfe.vers";
    public static final String PARAM_PFE_CLINAME           = "mcss.pfe.cliname";
    public static final String PARAM_PFE_SVCNAME_FULLINFO  = "mcss.pfe.svcname.fullinfo";
    public static final String PARAM_PFE_SVCNAME_DEBIT     = "mcss.pfe.svcname.debit";
    public static final String PARAM_PFE_SVCNAME_MOD_DEBIT = "mcss.pfe.svcname.mod_debit";
    public static final String PARAM_PFE_SVCNAME_CHECKCREDIT = "mcss.pfe.svcname.checkcredit";
    public static final String PARAM_PFE_TID               = "mcss.pfe.tid";
    public static final String PARAM_PFE_CONNMOD           = "mcss.pfe.connmod";
    
    public static final String PARAM_PFE_NOT_FOUND_MSISDN_CODE     = "mcss.pfe.not.found.msisdn.code";

    public static final String PARAM_SOCKETADAPTER_MAX_ACTIVE ="mcss.socket.adapter.pool.maxActive.";
    public static final String PARAM_SOCKETADAPTER_EXHAUSTED_ACTION ="mcss.socket.adapter.pool.whenExhaustedAction.";
    public static final String PARAM_SOCKETADAPTER_MAX_WAIT ="mcss.socket.adapter.pool.maxWait.";
    public static final String PARAM_SOCKETADAPTER_MAX_IDLE ="mcss.socket.adapter.pool.maxIdle.";
    public static final String PARAM_SOCKETADAPTER_MIN_IDLE ="mcss.socket.adapter.pool.minIdle.";

    public static final String PARAM_SOCKETADAPTER_CONNECTION_MAX_RETRY 	= "mcss.socket.adapter.connection.max.retry.";
    public static final String PARAM_SOCKETADAPTER_REQUEST_MAX_RETRY	= "mcss.socket.adapter.request.max.retry.";
    public static final String PARAM_SOCKETADAPTER_REMOTE_PORT_MAX_RETRY	= "mcss.socket.adapter.remote.port.max.retry.";
    public static final String PARAM_SOCKETADAPTER_SERVICE_NAME = "mcss.socket.adapter.service.name.";
//    public final static String PARAM_SOCKETADAPTER_LOCAL_ADDRESS = "mcss.socket.adapter.local.address";
//    public final static String PARAM_SOCKETADAPTER_ADDRESSES = "mcss.socket.adapter.addresses";
//    public final static String PARAM_SOCKETADAPTER_PORTS = "mcss.socket.adapter.ports";
//    public final static String PARAM_OPSC_PATTERNS = "mcss.socket.adapter.patterns";
//    public final static String PARAM_OPSC_IDS = "mcss.socket.adapter.ids";
//    public final static String PARAM_OPSC_RESPONSE_TIMEOUT = "mcss.socket.adapter.response.timeout";
//    public final static String PARAM_OPSC_CONNECT_WAIT = "mcss.socket.adapter.connect.wait";
    public final static String PARAM_OPSC_DEFAULT_BASKET = "mcss.prepaid.balance.default.basket";
//    public final static String PARAM_OPSC_PROTOCOL = "mcss.socket.adapter.protocol";
    public final static String PARAM_OPSC_FINAL_ERRORS = "mcss.opsc.final.errors";
    public final static String PARAM_OPSC_NET_STATE_BLOCK_VALUE = "mcss.opsc.net.state.block.value";
//    public final static String PARAM_OPSC_XML_TAG_STATE_BLOCK = "mcss.socket.adapter.xml.tag.state.block";

    public static final String PARAM_SOCKET_END_TOKEN = "mcss.socket.end.token.";
    public static final String PARAM_SOCKET_ADDRESS = "mcss.socket.address.";
    public static final String PARAM_SOCKET_BUFFER_SIZE = "mcss.socket.buffer.size.";
    public static final String PARAM_SOCKET_TCP_NODELAY = "mcss.socket.tcp.nodelay.";
    public static final String PARAM_SOCKET_USE_END_TOKEN = "mcss.socket.use.end.token.";
    public static final String PARAM_SOCKET_SO_LINGER = "mcss.socket.so.linger.";
    public static final String PARAM_SOCKET_REUSE_ADDRESS = "mcss.socket.reuse.address.";
    public static final String PARAM_SOCKET_SO_TIMEOUT = "mcss.socket.so.timeout.";
    public static final String PARAM_SOCKET_RECEIVE_BUFFER_SIZE = "mcss.socket.receive.buffer.size.";
    public static final String PARAM_SOCKET_KEEP_ALIVE = "mcss.socket.keep.alive.";
    public static final String PARAM_SOCKET_LOCAL_ADDRESS = "mcss.socket.local.address.";
    public static final String PARAM_SOCKET_CREATION_TIMEOUT = "mcss.socket.creation.timeout.";
    public static final String PARAM_SOCKET_MAX_READ_CYCLE = "mcss.socket.max.read.cycle.";
    public static final String PARAM_SOCKET_LOCAL_PORT = "mcss.socket.local.port.";
    public static final String PARAM_SOCKET_PORT = "mcss.socket.port.";

    public static final String PARAM_PROTOCOL_SOCKET_OPSC = "mcss.protocol.socket.opsc";

    public static final String PARAM_TRACE_WRONG_SOCKET_RESPONSE = "mcss.socket.adapter.trace.wrong.response.";

    public static final String PARAM_EIS_PRODUCT_NAME ="mcss.eis.product.name.";
    public static final String PARAM_EIS_PRODUCT_VERSION="mcss.eis.product.version.";
    public static final String PARAM_EIS_USER_NAME="mcss.eis.user.name.";
    public static final String PARAM_EIS_MAX_CONNECTIONS="mcss.eis.max.connctions.";

    public static final String PARAM_ADAPTER_VENDOR_NAME="mcss.adapter.vendor.name.";
    public static final String PARAM_ADAPTER_NAME="mcss.adapter.name.";
    public static final String PARAM_ADAPTER_SHORT_DESCRIPTION="mcss.adapter.short.description.";
    public static final String PARAM_ADAPTER_SPEC_VERSION="mcss.adapter.spec.version.";

    /** nome dei parametri necessari al BEAN INFOBUS/OPSC */
    public final static String PARAM_OPSC_MSG_SET = "mcss.opsc.msg.set.";
    public final static String PARAM_OPSC_OK_MSG = "mcss.opsc.msg.ok.value";
    public final static String PARAM_OPSC_LOW_CREDIT_MSG = "mcss.opsc.msg.low.credit.value";

    /** Servidor De Lineas **/
    public final static String PARAM_SDL_ERROR_CODES = "mcss.sdl.error.code.set";
    public final static String PARAM_SDL_ERROR_CODE_ABSENT_MSISDN = "mcss.sdl.error.code.absent.msisdn";
    public final static String PARAM_SDL_ENABLED	= "mcss.sdl.enabled";
    public final static String PARAM_SDL_MANDATORY  = "mcss.sdl.mandatory";
    public final static String PARAM_LOG4J_SDLLOGGER = "log4j.logger.sdlLogger";
    
    /** EMBEDDED Servidor De Lineas **/
    public final static String PARAM_LSDL_PORT 			= "lsdl.port";
    public final static String PARAM_LSDL_CACHE_NAME	= "lsdl.cache.name";
    public final static String PARAM_LSDL_LINES_FILE	= "lsdl.lines.file";
    public final static String PARAM_LSDL_LISTEN_IP		= "lsdl.listen.ip";
    public final static String PARAM_LSDL_NO_CACHE		= "lsdl.no.cahe";
    public final static String PARAM_LSDL_MSISDN_FILTER	= "lsdl.msisdn.filter";
    
    
    /** Codici di Risposta Servidor De Lineas **/
    /*
     * Aggiungere in configurazione le propriet�:
     * mcss.sdl.line.A=post
     * mcss.sdl.line.B=post
     * mcss.sdl.line.O=pre
     * mcss.sdl.line.Q=pre
     * mcss.sdl.line.SIE=??? (Verificare con il Cliente)
     * 
     */
    public final static String PARAM_SDL_LINETYPE = "mcss.sdl.line.";

    
    /** BSCS Parameters */
    public final static String PARAM_BSCS_WSDL_URL = "mcss.bscs.wsdl.url";
    public final static String PARAM_BSCS_OK_RETURN = "mcss.bscs.ok.return";
    public final static String PARAM_BSCS_BLOCKED_RETURN = "mcss.bscs.blocked.return";
    public final static String PARAM_BSCS_MSISDN_NOT_FOUND_RETURN = "mcss.bscs.msisdn.not.found.return";
    
    public final static String  PARAM_BSCS_SERVICE_USR = "mcss.bscs.service.usr";
    public final static String  PARAM_BSCS_SERVICE_PWD = "mcss.bscs.service.pwd";
    
    /** Gisp Returns */    
    public final static String PARAM_GISP_PROTOCOL = "mcss.gisp.protocol";
    public final static String PARAM_GISP_FULL_MSG_SET = "mcss.gisp.full.msg.set.";
    public final static String PARAM_GISP_FULL_OK_MSG = "mcss.gisp.full.msg.ok.value";
    public final static String PARAM_GISP_FULL_ABSENT_MSG = "mcss.gisp.full.absent.msg.value";
    public final static String PARAM_GISP_FULL_ERROR_MSG = "mcss.gisp.full.error.msg.value";
    public final static String PARAM_GISP_VIRT_MSG_SET = "mcss.gisp.virtual.msg.set.";
    public final static String PARAM_GISP_VIRT_OK_MSG = "mcss.gisp.virtual.msg.ok.value";
    public final static String PARAM_GISP_VIRT_ABSENT_MSG = "mcss.gisp.virtual.absent.msg.value";
    public final static String PARAM_GISP_VIRT_ERROR_MSG = "mcss.gisp.virtual.error.msg.value";
    public final static String PARAM_GISP_CAS_NAME_PAR = "mcss.gisp.cas.name.par";
    public final static String PARAM_GISP_CAS_NAME_VAL = "mcss.gisp.cas.name.val";
    public final static String PARAM_GISP_REQUEST_NAME_PAR = "mcss.gisp.request.name.par";
    public final static String PARAM_GISP_REQUEST_NAME_FULL_VAL = "mcss.gisp.request.name.full.val";
    public final static String PARAM_GISP_REQUEST_NAME_VIRT_VAL = "mcss.gisp.request.name.virtual.val";
    public final static String PARAM_GISP_NUM_TEL_PAR = "mcss.gisp.num.tel.par";
    public final static String PARAM_EJB_IBUS_GW_URL = "mcss.ejb.infobus.gw.url";
    public final static String PARAM_EJB_IBUS_GW_JNDI = "mcss.ejb.infobus.gw.jndi";
    public final static String PARAM_EJB_IBUS_GW_HOME = "mcss.ejb.infobus.gw.home";
    public final static String PARAM_OPSC_ID_SERV = "mcss.socket.adapter.ibus.id.balance";
    public final static String PARAM_OPSC_ID_SYSTEM = "mcss.socket.adapter.ibus.idsystem";
    public final static String PARAM_GISP_STAT_SERV = "mcss.gisp.ibus.id.stat";
    public final static String PARAM_GISP_VIR_SERV = "mcss.gisp.ibus.id.virtual";
    public final static String PARAM_GISP_ID_SYSTEM = "mcss.gisp.ibus.idsystem";
    public final static String PARAM_IBUS_ID_SYS = "mcss.ibus.idsystem";
    public final static String PARAM_IBUS_ID_COMMIT = "mcss.ibus.id.commit";
    public final static String PARAM_IBUS_APPDEP_1 = "mcss.ibus.app.dep1";
    public final static String PARAM_IBUS_APPDEP_1_CREDIT = "mcss.ibus.app.dep1.credit";
    public final static String PARAM_IBUS_NUM_COMMIT_RETRY = "mcss.ibus.num.commit.retry";
    public final static String PARAM_IBUS_COMMIT_BODY = "mcss.ibus.commit.body";

    /** Parameters for Hot Reload Operation */
    public final static String PARAM_RELOAD_SKIP_CHECK_IP = "mcss.reload.skip.check.ip";
    public final static String PARAM_RELOAD_TRUSTED_IP = "mcss.reload.trusted.ip";
    public final static String PARAM_RELOAD_ALL_COMPONENTS = "mcss.reload.all.components";
    
    /** SFTP Parameters **/
    public final static String PARAM_SFTP_ACTIVE = "mcss.sftp.active";
    
    public final static String PARAM_SFTP_REMOTE_IP = "mcss.sftp.remote.ip";
    public final static String PARAM_SFTP_REMOTE_USR = "mcss.sftp.remote.usr";											
    public final static String PARAM_SFTP_REMOTE_PWD = "mcss.sftp.remote.pwd";
    public final static String PARAM_SFTP_REMOTE_PATH = "mcss.sftp.remote.path";
    public final static String PARAM_SFTP_LOCAL_PATH = "mcss.sftp.local.path";
    public final static String PARAM_SFTP_FILTER = "mcss.sftp.filter";
    public final static String PARAM_SFTP_FILTER1 = "mcss.sftp.filter1";
    public final static String PARAM_SFTP_MAX_NUM_FILES = "mcss.sftp.max.num.files";
    
    public static String PARAM_SFTP_SSH_CERTIFICATO_FILE = "mcss.sftp.ssh.certificate.file"; //"./.ssh/id_dsa";
    
    /** ASN1 Controller **/
    public final static String PARAM_ASN1_SLEEP_SECONDS = "mcss.asn1.sleep.seconds";
    public final static String PARAM_ASN1_EXEC_STRING = "mcss.asn1.exec.string";
    public final static String PARAM_ASN1_CHECK_STRING = "mcss.asn1.check.string";
    
    /** nome dei parametri necessari al Bulk Loader */
    public final static String REGEX_BULK_FILTER_FILE = "mcss.bulk.filter.file";

    public final static String PARAM_SKIP_CHECK_IP = "mcss.skip.check.ip";
    public final static String PARAM_BULK_LOADER_TRUSTED_ID = "mcss.bulk.loader.trusted.id";
    public final static String PARAM_BULK_LOADER_ROOT_PATH = "mcss.bulk.loader.root.path";
    public final static String PARAM_BULK_LOADER_INPUT_DIRECTORY = "mcss.bulk.loader.input.directory";
    public final static String PARAM_BULK_LOADER_BACKUP_DIRECTORY = "mcss.bulk.loader.backup.directory";
    public final static String PARAM_BULK_LOADER_FAILED_DIRECTORY = "mcss.bulk.loader.failed.directory";
    public final static String PARAM_BULK_LOADER_FATAL_DIRECTORY = "mcss.bulk.loader.fatal.directory";
    public final static String PARAM_BULK_FOLDER_ADD_PATH = "mcss.bulk.folder.add.path";
    public final static String PARAM_BULK_FOLDER_SLEEP_SECONDS = "mcss.bulk.folder.sleep.seconds";
    public final static String PARAM_BULK_FOLDER_TABLENAME = "mcss.bulk.folder.tablename";
    public final static String PARAM_BULK_FOLDER_TABLEFIELD_LOADERID = "mcss.bulk.folder.tablefield.serverid";
    public final static String PARAM_BULK_FOLDER_TABLEFIELD_TYPE = "mcss.bulk.folder.tablefield.type";
    public final static String PARAM_BULK_FOLDER_TABLEFIELD_NAME = "mcss.bulk.folder.tablefield.name";
    public final static String PARAM_BULK_MAXRETRY = "mcss.bulk.param.maxretry";
    public final static String PARAM_BULK_MAXRESULTSET = "mcss.bulk.param.maxresultset";
    public final static String PARAM_BULK_TIMEOUT_TRANSACTION = "mcss.bulk.param.timeout.transaction";
    public final static String PARAM_BULK_BLOCKSIZE = "mcss.bulk.param.blocksize";
    public final static String PARAM_BULK_READER_SLEEP_SECONDS = "mcss.bulk.reader.sleep.seconds";
    public final static String PARAM_BULK_PROCESSOR_SLEEP_SECONDS = "mcss.bulk.processor.sleep.seconds";
    public final static String PARAM_BULK_READER_PREFIX_FILENAME = "mcss.bulk.reader.prefix.filename";
    public final static String PARAM_BULK_READER_EXTENSION_FILENAME = "mcss.bulk.reader.extension.filename";
    public final static String PARAM_TYPE_USER_2IN1A = "mcss.type.user.twoinonea";
    public final static String PARAM_TYPE_USER_2IN1B = "mcss.type.user.twoinoneb";
    public final static String PARAM_TYPE_PP = "mcss.type.user.pp";
    public final static String PARAM_TYPE_USER_PP2IN1A = "mcss.type.user.pptwoinonea";
    public final static String PARAM_TYPE_USER_PP2IN1B = "mcss.type.user.pptwoinoneb";
    public final static String PARAM_TYPE_DEFABB = "mcss.type.user.defabb";
    public final static String PARAM_TYPE_USER_ABB2IN1A = "mcss.type.user.abbtwoinonea";
    public final static String PARAM_TYPE_USER_ABB2IN1B = "mcss.type.user.abbtwoinoneb";
    public final static String PARAM_TYPE_USER_ABBTFA = "mcss.type.user.abbtfa";
    public final static String PARAM_TYPE_USER_ABBTBU = "mcss.type.user.abbtbu";
    public final static String PARAM_TYPE_USER_ABBTMB = "mcss.type.user.abbtmb";
    public final static String PARAM_TYPE_USER_ABBCO = "mcss.type.user.abbco";
    public final static String PARAM_TYPE_USER_ABBBU = "mcss.type.user.abbbu";
    public final static String PARAM_TYPE_USER_ABBMB = "mcss.type.user.abbmb";
    public final static String PARAM_TYPE_USER_UMTSMB = "mcss.type.user.umtsmb";
    public final static String PARAM_TYPE_USER_UMTSCO = "mcss.type.user.umtsco";
    public final static String PARAM_TYPE_USER_UMTSBU = "mcss.type.user.umtsbu";


    public final static String PARAM_TYPE_USER_AOM = "mcss.type.user.aom";
    public final static String PARAM_TYPE_RESPONSE_T = "mcss.type.response.t";
    public final static String PARAM_TYPE_RESPONSE_P = "mcss.type.response.p";
    public final static String PARAM_TYPE_RESPONSE_S = "mcss.type.response.s";

    /* Tells if MCSS is performing HTTP or HTTPS calls */
    public final static String PARAM_IS_HTTPS_MNP_CLIENT = "mcss.is.https.mnp.client";
    public final static String PARAM_IS_HTTPS_UVI_CLIENT = "mcss.is.https.uvi.client";
    public final static String PARAM_IS_HTTPS_GWC_CLIENT = "mcss.is.https.gwc.client";
    public final static String PARAM_IS_HTTPS_BSCS_CLIENT = "mcss.is.https.bscs.client";

    public final static String PARAM_CLIENT_CERT_UNITIM_FILE = "mcss.client.unitim.cert.file";
    public final static String PARAM_CLIENT_CERT_UNITIM_PWD = "mcss.client.unitim.cert.pwd";
    public final static String PARAM_CLIENT_SERVER_UNITIM_FILE = "mcss.server.unitim.cert.file";

    public final static String PARAM_CLIENT_CERT_UVI_FILE = "mcss.client.uvi.cert.file";
    public final static String PARAM_CLIENT_CERT_UVI_PWD = "mcss.client.uvi.cert.pwd";
    public final static String PARAM_CLIENT_SERVER_UVI_FILE = "mcss.server.uvi.cert.file";

    public final static String PARAM_CLIENT_CERT_SMSGW_FILE = "mcss.client.smsgw.cert.file";
    public final static String PARAM_CLIENT_CERT_SMSGW_PWD = "mcss.client.smsgw.cert.pwd";
    public final static String PARAM_CLIENT_SERVER_SMSGW_FILE = "mcss.server.smsgw.cert.file";
    
    public final static String PARAM_CLIENT_CERT_BSCS_FILE = "mcss.client.bscs.cert.file";
    public final static String PARAM_CLIENT_CERT_BSCS_PWD = "mcss.client.bscs.cert.pwd";
    public final static String PARAM_CLIENT_SERVER_BSCS_FILE = "mcss.server.bscs.cert.file";

    public final static String PARAM_SSL_VERBOSE_DEBUG = "mcss.ssl.verbose.debug";
    public final static String PARAM_STRICT_VERIFY = "mcss.ssl.hostname.strict.verify";


    /** Nomi e parametri delle servlet. */
    public final static String PARAM_UNITIM_URL = "mcss.unitim.url";
    public final static String PARAM_UNITIM_HTTPS_URL = "mcss.unitim.https.url";
    public final static String PARAM_UNITIM_TIMEOUT = "mcss.unitim.timeout";
    public final static String PARAM_UNITIM_NUMTEL = "mcss.unitim.param.numtel";
    public final static String PARAM_UNITIM_CUSTCODE = "mcss.unitim.param.custcode";
    public final static String PARAM_UVITOMSISDN_URL = "mcss.uvitomsisdn.url";
    public final static String PARAM_UVITOMSISDN_HTTPS_URL = "mcss.uvitomsisdn.https.url";
    public final static String PARAM_UVITOMSISDN_UVI = "mcss.uvitomsisdn.uvi";

    /* Parameters for Queue Locator */
	// Credit Reservation
    public final static String PARAM_JMS_RESERVATION_SERVER_FACTORY = "mcss.jms.reservation.server.factory";
    public final static String PARAM_JMS_RESERVATION_SERVER_URL = "mcss.jms.reservation.server.url";
    public final static String PARAM_JMS_RESERVATION_CONNECTION_FACTORY = "mcss.jms.reservation.connection.factory";
    public final static String PARAM_JMS_RESERVATION_QUEUE_NAME = "mcss.jms.reservation.queue.name";
	// Billing Store And Forward
    public final static String PARAM_JMS_BILLING_SERVER_FACTORY = "mcss.jms.billing.server.factory";
    public final static String PARAM_JMS_BILLING_SERVER_URL = "mcss.jms.billing.server.url";
    public final static String PARAM_JMS_BILLING_CONNECTION_FACTORY = "mcss.jms.billing.connection.factory";
    public final static String PARAM_JMS_BILLING_QUEUE_NAME = "mcss.jms.billing.queue.name";
	// Failed Billing S&F
    public final static String PARAM_JMS_FAILED_BILL_SERVER_FACTORY 	= "mcss.jms.failed.bill.server.factory";
    public final static String PARAM_JMS_FAILED_BILL_SERVER_URL 		= "mcss.jms.failed.bill.server.url";
    public final static String PARAM_JMS_FAILED_BILL_CONNECTION_FACTORY = "mcss.jms.failed.bill.connection.factory";
    public final static String PARAM_JMS_FAILED_BILL_QUEUE_NAME 		= "mcss.jms.failed.bill.queue.name";

    /* Inizio Parametri UDR */
    public final static String PARAM_SERV_CHARGE_TYPE_VOLUME = "mcss.serv.charge.type.volume";
    public final static String PARAM_CHANNEL_TYPE_PTP = "mcss.ChannelType.ptp";
    public final static String PARAM_CHANNEL_TYPE_TRIGGER = "mcss.ChannelType.trigger";
    public final static String PARAM_USERHPLMNIDENTIFIER_PTP = "mcss.userHPLMNIdentifier.ptp";
    public final static String PARAM_USERHPLMNIDENTIFIER_TRIGGER = "mcss.userHPLMNIdentifier.trigger";
	    public final static String PARAM_RECIPIENTHPLMNIDENTIFIER_TRIGGER = "mcss.recipientHPLMNIdentifier.trigger";
    public final static String PARAM_MAX_BYTES_VOLUME = "mcss.max.bytes.volume";
    /* Fine Parametri UDR */

    /* SMS Caring Parameters - Begin */
    public final static String PARAM_JMS_OUTBOUND_SMS_SERVER_FACTORY = "mcss.jms.outbound.sms.server.factory";
    public final static String PARAM_JMS_OUTBOUND_SMS_SERVER_URL = "mcss.jms.outbound.sms.server.url";
    public final static String PARAM_JMS_OUTBOUND_SMS_CONNECTION_FACTORY = "mcss.jms.outbound.sms.connection.factory";
    public final static String PARAM_JMS_OUTBOUND_SMS_QUEUE_NAME = "mcss.jms.outbound.sms.queue.name";

    /* GISP RIF SERVICE LOCKING */
    public final static String PARAM_LOCKED_GSM_RIF = "mcss.locked.gsm.rif";
    public final static String PARAM_LOCKED_TACS_RIF = "mcss.locked.tacs.rif";

    public final static String PARAM_TACS_SERVICE = "mcss.tacs.rif.service";
    public final static String PARAM_GSM_SERVICE = "mcss.gsm.rif.service";

    // HTTP GateWay
    public final static String PARAM_OUTBOUND_URL 		= "mcss.outbound.url";
    public final static String PARAM_OUTBOUND_HTTPS_URL 		= " mcss.outbound.https.url";
    public final static String PARAM_OUTBOUND_STATUS_TAG     	= "mcss.outbound.status.tag";
    public final static String PARAM_OUTBOUND_SUCCESS        	= "mcss.outbound.success";
    public final static String PARAM_OUTBOUND_MESSAGE        	= "mcss.outbound.message";
	    public final static String PARAM_OUTBOUND_OADC				= "mcss.outbound.oadc";
    public final static String PARAM_OUTBOUND_MESSAGE_ID     	= "mcss.outbound.message.identifier";
    public final static String PARAM_OUTBOUND_ASSIGNED_MESSAGE_ID = "mcss.outbound.assigned.identifier";
    public final static String PARAM_OUTBOUND_MSISDN         	= "mcss.outbound.msisdn";
    public final static String PARAM_OUTBOUND_CONSTANT_QUERY 	= "mcss.outbound.constant.query";
    /* SMS Caring Parameters - End */

	/* Set Check/Don't Check Unitim for Bulk */
	public final static String PARAM_BULK_UNITIM_CHK 	= "mcss.bulk.unitim.chk";

	/** Inizio Configurazione statistiche */
	public final static String PARAM_STAT_SEPARATOR_ONE                            = "mcss.stat.separator.one";
	public final static String PARAM_STAT_SEPARATOR_TWO                            = "mcss.stat.separator.two";
	public final static String PARAM_STAT_KEY_NOT_VALIDATED                        = "mcss.stat.key.not.validated";
	public final static String PARAM_STAT_CHECK_AUTH_IN                            = "mcss.stat.check.auth.in";
	public final static String PARAM_STAT_KEY_INPUT_NOT_OK_AUTH                    = "mcss.stat.key.input.not.ok.auth";
	public final static String PARAM_STAT_KEY_INPUT_OK_AUTH                        = "mcss.stat.key.input.ok.auth";
	public final static String PARAM_STAT_KEY_OUT_AUTH_NOT_OK                      = "mcss.stat.key.out.auth.not.ok";
	public final static String PARAM_STAT_KEY_OUT_AUTH_OK                          = "mcss.stat.key.out.auth.ok";
	public final static String PARAM_STAT_KEY_OUT_RES_BALANCE_OUT                  = "mcss.stat.key.out.res.balance.out";
	public final static String PARAM_STAT_KEY_OUT_RES_BALANCE_COMMIT               = "mcss.stat.key.out.res.balance.commit";
	public final static String PARAM_STAT_KEY_OUT_RES_BALANCE_OK                   = "mcss.stat.key.out.res.balance.ok";
	public final static String PARAM_STAT_KEY_OUT_RES_BALANCE_KO                   = "mcss.stat.key.out.res.balance.ko";
	public final static String PARAM_STAT_KEY_OUT_RES_BALANCE_TIMEOUT              = "mcss.stat.key.out.res.balance.timeout";
	public final static String PARAM_STAT_KEY_OUT_AUTH_ICARD_OUT                   = "mcss.stat.key.out.auth.icard.out";
	public final static String PARAM_STAT_KEY_OUT_AUTH_ICARD_KO                    = "mcss.stat.key.out.auth.icard.ko";
	public final static String PARAM_STAT_KEY_OUT_AUTH_ICARD_OK                    = "mcss.stat.key.out.auth.icard.ok";
	public final static String PARAM_STAT_KEY_INPUT_NOT_OK_BILL                    = "mcss.stat.key.input.not.ok.bill";
	public final static String PARAM_STAT_KEY_INPUT_NOT_OK_BULK_BILL               = "mcss.stat.key.input.not.ok.bulk.bill";
	public final static String PARAM_STAT_KEY_INPUT_OK_BILL                        = "mcss.stat.key.input.ok.bill";
	public final static String PARAM_STAT_KEY_INPUT_OK_BULK_BILL                   = "mcss.stat.key.input.ok.bulk.bill";
	public final static String PARAM_STAT_KEY_OUT_BILL_NOT_OK                      = "mcss.stat.key.out.bill.not.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_OK_UDR                      = "mcss.stat.key.out.bill.ok.udr";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_OUT                 = "mcss.stat.key.out.bill.balance.out";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_COMMIT              = "mcss.stat.key.out.bill.balance.commit";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_OK                  = "mcss.stat.key.out.bill.balance.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_KO                  = "mcss.stat.key.out.bill.balance.ko";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_TIMEOUT             = "mcss.stat.key.out.bill.balance.timeout";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_QUEUE               = "mcss.stat.key.out.bill.balance.queue";
	public final static String PARAM_STAT_KEY_OUT_BILL_BULK_NOT_OK                 = "mcss.stat.key.out.bill.bulk.not.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_OK_BULK_UDR                 = "mcss.stat.key.out.bill.ok.bulk.udr";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_OUT            = "mcss.stat.key.out.bill.balance.bulk.out";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_COMMIT         = "mcss.stat.key.out.bill.balance.bulk.commit";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_OK             = "mcss.stat.key.out.bill.balance.bulk.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_KO             = "mcss.stat.key.out.bill.balance.bulk.ko";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_TIMEOUT        = "mcss.stat.key.out.bill.balance.bulk.timeout";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_QUEUE          = "mcss.stat.key.out.bill.balance.bulk.queue";
	public final static String PARAM_STAT_KEY_OUT_BILL_BULK_RESPONSE_NOT_OK        = "mcss.stat.key.out.bill.bulk.response.not.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_SCARTO_BULK_NOT_OK          = "mcss.stat.key.out.bill.scarto.bulk.not.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_SCARTO_FATAL_BULK_NOT_OK    = "mcss.stat.key.out.bill.scarto.fatal.bulk.not.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_SCARTO_BULK_FATAL           = "mcss.stat.key.out.bill.scarto.bulk.fatal";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_PLUS_UDR_OK         = "mcss.stat.key.out.bill.balance.plus.udr.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_PLUS_UDR_OK    = "mcss.stat.key.out.bill.balance.bulk.plus.udr.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_MINUS_UDR_OK        = "mcss.stat.key.out.bill.balance.minus.udr.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_MINUS_UDR_OK   = "mcss.stat.key.out.bill.balance.bulk.minus.udr.ok";
	public final static String PARAM_STAT_KEY_OUT_ASN1_OK                          = "mcss.stat.key.out.asn1.ok";
	public final static String PARAM_STAT_KEY_OUT_ASN1_FILE_NAME                   = "mcss.stat.key.out.asn1.file.name";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_OUT          = "mcss.stat.key.out.on.mess.res.balance.out";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_COMMIT       = "mcss.stat.key.out.on.mess.res.balance.commit";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_OK           = "mcss.stat.key.out.on.mess.res.balance.ok";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_BILLED_OK    = "mcss.stat.key.out.on.mess.res.balance.billed.ok";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_KO           = "mcss.stat.key.out.on.mess.res.balance.ko";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_TIMEOUT      = "mcss.stat.key.out.on.mess.res.balance.timeout";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_QUEUE        = "mcss.stat.key.out.on.mess.res.balance.queue";
	public final static String PARAM_STAT_KEY_OUT_ON_MESS_RES_BILL_BALANCE_ACTIVE  = "mcss.stat.key.out.on.mess.res.bill.balance.active";
	public final static String PARAM_STAT_KEY_OUT_BILL_ICARD_OUT                   = "mcss.stat.key.out.bill.icard.out";
	public final static String PARAM_STAT_KEY_OUT_BILL_ICARD_KO                    = "mcss.stat.key.out.bill.icard.ko";
	public final static String PARAM_STAT_KEY_OUT_BILL_ICARD_OK                    = "mcss.stat.key.out.bill.icard.ok";
	public final static String PARAM_STAT_KEY_OUT_BILL_OK_ICARD_CH                 = "mcss.stat.key.out.bill.ok.icard.ch";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_NOT_OK_AUTH		   = "mcss.stat.key.input.parameter.not.ok.auth";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_NOT_OK_BILL		   = "mcss.stat.key.input.parameter.not.ok.bill";
	public final static String PARAM_STAT_KEY_INPUT_BILLING_WITH_AUTH   		   = "mcss.stat.key.input.billing.with.auth";

	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_GISP_FULL			   = "mcss.stat.key.input.parameter.gisp.full";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_GISP_21			   = "mcss.stat.key.input.parameter.gisp.21";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_UNITIM		  	   = "mcss.stat.key.input.parameter.unitim";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_UVI		  		   = "mcss.stat.key.input.parameter.uvi";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_OPSC				   = "mcss.stat.key.input.parameter.opsc";
	public final static String PARAM_STAT_KEY_INPUT_PARAMETER_SMS_CARING		   = "mcss.stat.key.input.parameter.sms.caring";

	public final static String PARAM_STAT_KEY_TYPE_OPERATION_DEC				   = "mcss.stat.key.type.operation.dec";
	public final static String PARAM_STAT_KEY_TYPE_OPERATION_ADD				   = "mcss.stat.key.type.operation.add";

	public final static String PARAM_STAT_KEY_RETRY_NOT_OK_BULK_BILL			   = "mcss.stat.key.retry.not.ok.bulk.bill";
	public final static String PARAM_STAT_KEY_RETRY_OK_BULK_BILL			   	   = "mcss.stat.key.retry.ok.bulk.bill";

	public final static String PARAM_STAT_KEY_RETRY_BILL_NOT_OK					   ="mcss.stat.key.retry.bill.not.ok";
	public final static String PARAM_STAT_KEY_CHANGE_JMS_REDELIVERED 			   ="mcss.stat.key.change.jms.redelivered";
	/** Fine Configurazione statistiche */
	
	/** Parametri per la gestione della Cache di Profile MSISDN */
	public final static String	PARAM_PREPAID_CACHE_TYPE						="mcss.prepaid.cache.type";
	public final static String	PARAM_POSTPAID_CACHE_TYPE						="mcss.postpaid.cache.type";
	
	public final static String	PARAM_COHERENCE_LOCK_TIMEOUT					="mcss.coherence.lock.timeout";
	public final static String	PARAM_MCSS_PREPAID_COHERENCE_CACHE_NAME			="mcss.prepaid.coherence.cache.name";
	public final static String	PARAM_MCSS_POSTPAID_COHERENCE_CACHE_NAME		="mcss.postpaid.coherence.cache.name";
	public final static String	PARAM_MCSS_SLA_COHERENCE_CACHE_NAME				="mcss.sla.coherence.cache.name";
	
	/** CACHE OPENSYNPHONY **/ 
	public final static String	PARAM_CHACHE_VALIDITY_TIME						="mcss.cache.validity.time";
	public final static String	PARAM_CHACHE_POSTPAID_VALIDITY_TIME				="mcss.cache.postpaid.validity.time";
	public final static String	PARAM_CHACHE_TID_VALIDITY_TIME					="mcss.cache.tid.validity.time";
	public final static String	PARAM_MCSS_AUTH_USE_PROFILE_CACHE				="mcss.auth.use.profile.cache";
	public final static String	PARAM_MCSS_INFO_USE_PROFILE_CACHE				="mcss.info.use.profile.cache";
	public final static String	PARAM_MCSS_CHKANDBILL_USE_PROFILE_CACHE			="mcss.chkandbill.use.profile.cache";
	public final static String	PARAM_MCSS_PROFILE_CHACHE_IS_BULK_ONLY			="mcss.profile.cache.is.bulk.only";
	
	public final static String	PARAM_MCSS_USE_PREPAID_CACHE					="mcss.use.prepaid.cache";
	public final static String	PARAM_MCSS_USE_POSTPAID_CACHE					="mcss.use.postpaid.cache";
	public final static String	PARAM_MCSS_USE_TID_CACHE						="mcss.use.tid.cache";
	
	public final static String PARAM_CACHE_POSTPAID_MAX_ELEM					="mcss.cache.postpaid.max.elem";
	public final static String PARAM_CACHE_POSTPAID_PERSITENCE_PATH				="mcss.cache.postpaid.persistence.path";
	public final static String PARAM_CACHE_POSTPAID_ALGORITHM					="mcss.cache.postpaid.algorithm";
	public final static String PARAM_CACHE_POSTPAID_PERSISTENCE_OVERFLOW_ONLY	="mcss.cache.postpaid.persistence.overflow.only";
	public final static String PARAM_CACHE_POSTPAID_MEMORY						="mcss.cache.postpaid.memory";
	public final static String PARAM_CACHE_POSTPAID_PERSISTENCE_CLASS			="mcss.cache.postpaid.persistence.class";
	
	public final static String PARAM_CACHE_PREPAID_MAX_ELEM						="mcss.cache.prepaid.max.elem";
	public final static String PARAM_CACHE_PREPAID_PERSITENCE_PATH				="mcss.cache.prepaid.persistence.path";
	public final static String PARAM_CACHE_PREPAID_ALGORITHM					="mcss.cache.prepaid.algorithm";
	public final static String PARAM_CACHE_PREPAID_PERSISTENCE_OVERFLOW_ONLY	="mcss.cache.prepaid.persistence.overflow.only";
	public final static String PARAM_CACHE_PREPAID_MEMORY						="mcss.cache.prepaid.memory";
	public final static String PARAM_CACHE_PREPAID_PERSISTENCE_CLASS			="mcss.cache.prepaid.persistence.class";
	
	public final static String PARAM_CACHE_TID_MAX_ELEM							="mcss.cache.tid.max.elem";
	public final static String PARAM_CACHE_TID_PERSITENCE_PATH					="mcss.cache.tid.persistence.path";
	public final static String PARAM_CACHE_TID_ALGORITHM						="mcss.cache.tid.algorithm";
	public final static String PARAM_CACHE_TID_PERSISTENCE_OVERFLOW_ONLY		="mcss.cache.tid.persistence.overflow.only";
	public final static String PARAM_CACHE_TID_MEMORY							="mcss.cache.tid.memory";
	public final static String PARAM_CACHE_TID_PERSISTENCE_CLASS				="mcss.cache.tid.persistence.class";
	
	public final static String	PARAM_MCSS_USE_UDR_PRICING						="mcss.use.udr.pricing";
	public final static String	PARAM_MCSS_USE_PP_COSTS							="mcss.use.pp.costs";
	
	//SDP ID ACCESS PARAMS
	public final static String	PARAM_SDP_SP_ID										="mcss.sdp.id";
	public final static String	PARAM_SDP_PWD										="mcss.sdp.pwd";
	public final static String	PARAM_SDP_SERVICE_ID								="mcss.sdp.service.id";
	public final static String	PARAM_SDP_PRODUCT_ID								="mcss.sdp.product.id";			

    /** Codici dei messaggi dell'applicazione */
    public final static String MSG_AUTHORIZATION_GRANTED							= "mcss.message.000.001";
    public final static String MSG_BILLING_REQUEST_ACCEPTED							= "mcss.message.000.002";
    public final static String MSG_CHECKTID_REQUEST_ACCEPTED						= "mcss.message.000.003";
    public final static String MSG_ROLLBACK_EXECUTED								= "mcss.message.000.004";
    public final static String MSG_RELOAD_OK										= "mcss.message.000.005";
    public final static String MSG_USER_ENABLED										= "mcss.message.000.006";
    public final static String MSG_RECYCLE_COMMAND_OK								= "mcss.message.000.007";
    public final static String MSG_RECYCLESMSO_COMMAND_OK							= "mcss.message.000.008";
    public final static String MSG_GRANTED_PARTIAL_BILLING							= "mcss.message.000.200";    
    public final static String MSG_UNKNOWN_CSP										= "mcss.message.001";
    public final static String MSG_CSP_NOT_ENABLED									= "mcss.message.002";
    public final static String MSG_UNKNOWN_USER										= "mcss.message.003";
    public final static String MSG_UNKNOWN_SERVICE									= "mcss.message.004";
    public final static String MSG_USER_NOT_ENABLED									= "mcss.message.005";
    public final static String MSG_NOT_ENOUGH_CREDIT								= "mcss.message.006";
    public final static String MSG_SYSTEM_NOT_AVAILABLE								= "mcss.message.007";
    public final static String MSG_SERVICE_NOT_AVAILABLE_MISSING_AUTHORIZATION_CODE	= "mcss.message.008";
    public final static String MSG_SERVICE_NOT_AVAILABLE_COST_NOT_COMPUTED			= "mcss.message.009";
    public final static String MSG_SERVICE_NOT_AVAILABLE_TOO_MUCH_RECIPIENTS		= "mcss.message.010";
    public final static String MSG_AUTHORIZATION_CODE_ALREADY_USED					= "mcss.message.011";
    public final static String MSG_AUTHORIZATION_NOT_VALID_FOR_RECIPIENT			= "mcss.message.012";
    public final static String MSG_REQUEST_CONTENTS_NOT_VALID						= "mcss.message.013";
    public final static String MSG_AUTHORIZATION_CODE_NOT_VALID						= "mcss.message.014";
    public final static String MSG_EXPIRED_AUTHORIZATION_CODE						= "mcss.message.015";
    public final static String MSG_CSP_IS_MANDATORY									= "mcss.message.016";
    public final static String MSG_CSP_USER_IS_MANDATORY							= "mcss.message.017";
    public final static String MSG_CSP_PASSWORD_IS_MANDATORY						= "mcss.message.018";
    public final static String MSG_RECIPIENT_IS_MANDATORY							= "mcss.message.019";
    public final static String MSG_MSISDN_IS_MANDATORY								= "mcss.message.020";
    public final static String MSG_SERVICE_ID_IS_MANDATORY							= "mcss.message.021";
    public final static String MSG_SERVICE_CLASS_IS_MANDATORY						= "mcss.message.022";
    public final static String MSG_EVENT_ID_IS_MANDATORY							= "mcss.message.023";
    public final static String MSG_WRONG_REQUEST									= "mcss.message.024";
    public final static String MSG_UNKNOWN_FUNCTION									= "mcss.message.025";
    public final static String MSG_UNITIM_SERVICE_NOT_AVAILABLE						= "mcss.message.026"; //Not Used
    public final static String MSG_UNITIM_SERVICE_WRONG_MSISDN_FORMAT				= "mcss.message.027";
    public final static String MSG_OPSC_SYSTEM_GENERIC_ERROR						= "mcss.message.028";
    public final static String MSG_TYPE_OF_CARD_NOT_QUALIFIED						= "mcss.message.029";
    public final static String MSG_WRONG_CONFIGURATION_OF_CARD_VAS					= "mcss.message.030"; //Not Used
    public final static String MSG_ERR_INFOBUS_ERROR								= "mcss.message.031"; //Not Used
    public final static String MSG_WARN_INFOBUS_NOT_AVAILABLE						= "mcss.message.032"; //Not Used
    public final static String MSG_NOT_POST_SERVICE									= "mcss.message.033"; //Not Used
    public final static String MSG_NOT_PRE_SERVICE									= "mcss.message.034";
    public final static String MSG_AUTHORIZATION_TID_NOT_UNIQUE						= "mcss.message.035";
    public final static String MSG_TID_IS_MANDATORY									= "mcss.message.036";
    public final static String MSG_TID_CODE_NOT_VALID								= "mcss.message.037";
    public final static String MSG_TID_RECORD_NOT_FOUND								= "mcss.message.038";
    public final static String MSG_WRONG_CONFIGURATION_TID							= "mcss.message.039";
    public final static String MSG_CSP_NOT_ENABLED_FOR_TID							= "mcss.message.040";
    public final static String MSG_NO_ACTIVE_RESERVATION							= "mcss.message.041";
    public final static String MSG_USER_IS_MOROSO									= "mcss.message.042"; //Not Used
    public final static String MSG_RELOAD_NOT_ALLOWED								= "mcss.message.044";
    public final static String MSG_RELOAD_FAILED									= "mcss.message.045";
    public final static String MSG_ERR_GISP_NOT_AVAILABLE							= "mcss.message.046"; //Not Used
    public final static String MSG_ERR_RET_CODE_GISP_UNKNOWN						= "mcss.message.047"; //Not Used
    public final static String MSG_USER_IS_STOPPED									= "mcss.message.048"; //Not Used
    public final static String MSG_USER_IS_BLOCKED									= "mcss.message.049";
    public final static String MSG_SYNTAX_ERROR_GISP_1								= "mcss.message.050"; //Not Used
    public final static String MSG_SYNTAX_ERROR_GISP_2								= "mcss.message.051"; //Not Used
    public final static String MSG_GISP_RESPONSE_NOT_OK								= "mcss.message.052"; //Not Used
    public final static String MSG_GISP_TOU_UNKNOWN									= "mcss.message.053";
    public final static String MSG_GISP21_NOT_AVAILABLE								= "mcss.message.054"; //Not Used
    public final static String MSG_TYPE_USER_NOT_ENABLED							= "mcss.message.055";
    public final static String MSG_USER_CF_NOT_ENABLE_FOR_SERVICE					= "mcss.message.056";
    public final static String MSG_USER_PRE_PRE_ACTIVATION							= "mcss.message.057";
    public final static String MSG_CUSTOMER_DISABLED_IN_NETWORK						= "mcss.message.058";
    public final static String MSG_CUSTOMER_BLOCKED_FOR_THEFT_LOSS					= "mcss.message.059"; //Not Used
    public final static String MSG_CUSTOMER_BLOCKED_FOR_ARREARS						= "mcss.message.060"; //Not Used
    public final static String MSG_CUSTOMER_BLOCKED_FOR_FRAUD						= "mcss.message.061"; //Not Used
    public final static String MSG_CUSTOMER_SMT_BLOCKED								= "mcss.message.062"; //Not Used
    public final static String MSG_CUSTOMER_SMO_BLOCKED								= "mcss.message.063"; //Not Used
    public final static String MSG_CUSTOMER_CARD_EXPIRED							= "mcss.message.064"; //Not Used
    public final static String MSG_CUSTOMER_RIF_CLOSED								= "mcss.message.065"; //Not Used
    public final static String MSG_CUSTOMER_CARD_PREACTIVED							= "mcss.message.066"; //Not Used
    public final static String MSG_CUSTOMER_CARD_CREDIT_ENDED						= "mcss.message.067"; //Not Used
    public final static String MSG_BILLING_NOT_ALLOWED								= "mcss.message.068";
    public final static String MSG_BSCS_SYSTEM_FAULT								= "mcss.message.069";       
    public final static String MSG_ERR_RET_CODE_NOT_IN_HAPPY_HOUR					= "mcss.message.070";
    public final static String MSG_ICARD_AUTHORIZATION_GRANTED						= "mcss.message.071";
    
    public final static String MSG_SDL_SYSTEM_GENERIC_ERROR							= "mcss.message.072";
    
    public final static String MSG_PIN_AUTHORIZATION_GRANTED 						= "mcss.message.101";
    public final static String MSG_PIN_GENERATED 									= "mcss.message.102";
    public final static String MSG_PIN_BURNED 										= "mcss.message.103";
    
    public final static String MSG_PIN_IS_MANDATORY 								= "mcss.message.104";
    public final static String MSG_PIN_IS_INVALID 									= "mcss.message.105";
    public final static String MSG_CMD_NOT_ALLOWED_FOR_PIN 							= "mcss.message.106";
    public final static String MSG_PIN_HAS_EXPIRED 									= "mcss.message.107";
    public final static String MSG_NO_ACTIVE_PIN									= "mcss.message.108";
    public final static String MSG_PIN_FALED_TO_SEND								= "mcss.message.109";    

    public final static String MSG_PARTIAL_BILLING_REQUREST_ACCEPTED				= "mcss.message.201";
    
    public final static String MSG_TEMPORARY_UNKNOWN_USER							= "mcss.message.202";
    
    public final static String MSG_SLA_EXCEEDED										= "mcss.message.301";
    
    /*
     * Inizio variabili per gestione tipologia messaggio, ogni variabile �
	 * ottenuta
     * dal corrispondente messaggio premettendo il suffisso "TYPE_"
     * es. MSG_XXX_YYY = "mcss.message.nnn" --> TYPE_MSG_XXX_YYY = "mcss.type.message.nnn";
     * 
	 */
    public final static String TYPE_MSG_AUTHORIZATION_GRANTED 						= "mcss.type.message.000.001";
    public final static String TYPE_MSG_BILLING_REQUEST_ACCEPTED 					= "mcss.type.message.000.002";
    public final static String TYPE_MSG_CHECKTID_REQUEST_ACCEPTED 					= "mcss.type.message.000.003";
    public final static String TYPE_MSG_ROLLBACK_EXECUTED 							= "mcss.type.message.000.004";
    public final static String TYPE_MSG_RELOAD_OK									= "mcss.type.message.000.005";
    public final static String TYPE_MSG_USER_ENABLED								= "mcss.type.message.000.006";
    public final static String TYPE_MSG_RECYCLE_COMMAND_OK							= "mcss.type.message.000.007";
    public final static String TYPE_MSG_RECYCLESMSO_COMMAND_OK						= "mcss.type.message.000.008";
    public final static String TYPE_MSG_GRANTED_PARTIAL_BILLING						= "mcss.type.message.000.200"; 
    public final static String TYPE_MSG_UNKNOWN_CSP									= "mcss.type.message.001";
    public final static String TYPE_MSG_CSP_NOT_ENABLED								= "mcss.type.message.002";
    public final static String TYPE_MSG_UNKNOWN_USER								= "mcss.type.message.003";
    public final static String TYPE_MSG_UNKNOWN_SERVICE								= "mcss.type.message.004";
    public final static String TYPE_MSG_USER_NOT_ENABLED							= "mcss.type.message.005";
    public final static String TYPE_MSG_NOT_ENOUGH_CREDIT							= "mcss.type.message.006";
    public final static String TYPE_MSG_SYSTEM_NOT_AVAILABLE						= "mcss.type.message.007";
    public final static String TYPE_MSG_SERVICE_MISSING_AUTHORIZATION_CODE			= "mcss.type.message.008";
    public final static String TYPE_MSG_SERVICE_NOT_AVAILABLE_COST_NOT_COMPUTED		= "mcss.type.message.009";
    public final static String TYPE_MSG_SERVICE_NOT_AVAILABLE_TOO_MUCH_RECIPIENTS	= "mcss.type.message.010";
    public final static String TYPE_MSG_AUTHORIZATION_CODE_ALREADY_USED				= "mcss.type.message.011";
    public final static String TYPE_MSG_AUTHORIZATION_NOT_VALID_FOR_RECIPIENT		= "mcss.type.message.012";
    public final static String TYPE_MSG_REQUEST_CONTENTS_NOT_VALID 			= "mcss.type.message.013";
    public final static String TYPE_MSG_AUTHORIZATION_CODE_NOT_VALID 		= "mcss.type.message.014";
    public final static String TYPE_MSG_EXPIRED_AUTHORIZATION_CODE 			= "mcss.type.message.015";
    public final static String TYPE_MSG_CSP_IS_MANDATORY 					= "mcss.type.message.016";
    public final static String TYPE_MSG_CSP_USER_IS_MANDATORY 				= "mcss.type.message.017";
    public final static String TYPE_MSG_CSP_PASSWORD_IS_MANDATORY 			= "mcss.type.message.018";
    public final static String TYPE_MSG_RECIPIENT_IS_MANDATORY 				= "mcss.type.message.019";
    public final static String TYPE_MSG_MSISDN_IS_MANDATORY 				= "mcss.type.message.020";
    public final static String TYPE_MSG_SERVICE_ID_IS_MANDATORY 			= "mcss.type.message.021";
    public final static String TYPE_MSG_SERVICE_CLASS_IS_MANDATORY 			= "mcss.type.message.022";
    public final static String TYPE_MSG_EVENT_ID_IS_MANDATORY 				= "mcss.type.message.023";
    public final static String TYPE_MSG_WRONG_REQUEST 						= "mcss.type.message.024";
    public final static String TYPE_MSG_UNKNOWN_FUNCTION 					= "mcss.type.message.025";
    public final static String TYPE_MSG_UNITIM_SERVICE_NOT_AVAILABLE 		= "mcss.type.message.026";
    public final static String TYPE_MSG_UNITIM_SERVICE_WRONG_MSISDN_FORMAT	= "mcss.type.message.027";
    public final static String TYPE_MSG_OPSC_SYSTEM_GENERIC_ERROR 			= "mcss.type.message.028";
    public final static String TYPE_MSG_TYPE_OF_CARD_NOT_QUALIFIED 			= "mcss.type.message.029";
    public final static String TYPE_MSG_WRONG_CONFIGURATION_OF_CARD_VAS 	= "mcss.type.message.030";
    public final static String TYPE_MSG_ERR_INFOBUS_ERROR 					= "mcss.type.message.031";
    public final static String TYPE_MSG_WARN_INFOBUS_NOT_AVAILABLE 			= "mcss.type.message.032";
    public final static String TYPE_MSG_NOT_POST_SERVICE 					= "mcss.type.message.033";
    public final static String TYPE_MSG_NOT_PRE_SERVICE 					= "mcss.type.message.034";
    public final static String TYPE_MSG_AUTHORIZATION_TID_NOT_UNIQUE 		= "mcss.type.message.035";
    public final static String TYPE_MSG_TID_IS_MANDATORY 					= "mcss.type.message.036";
    public final static String TYPE_MSG_TID_CODE_NOT_VALID 					= "mcss.type.message.037";
    public final static String TYPE_MSG_TID_RECORD_NOT_FOUND				= "mcss.type.message.038";
    public final static String TYPE_MSG_WRONG_CONFIGURATION_TID				= "mcss.type.message.039";
    public final static String TYPE_MSG_CSP_NOT_ENABLED_FOR_TID 			= "mcss.type.message.040";
    public final static String TYPE_MSG_NO_ACTIVE_RESERVATION 				= "mcss.type.message.041";
    public final static String TYPE_MSG_USER_IS_MOROSO 						= "mcss.type.message.042";
    public final static String TYPE_MSG_RELOAD_NOT_ALLOWED 					= "mcss.type.message.044";
    public final static String TYPE_MSG_RELOAD_FAILED 						= "mcss.type.message.045";
    public final static String TYPE_MSG_ERR_GISP_NOT_AVAILABLE 				= "mcss.type.message.046";
    public final static String TYPE_MSG_ERR_RET_CODE_GISP_UNKNOWN 			= "mcss.type.message.047";
    public final static String TYPE_MSG_USER_IS_STOPPED 					= "mcss.type.message.048";
    public final static String TYPE_MSG_USER_IS_BLOCKED 					= "mcss.type.message.049";
    public final static String TYPE_MSG_SYNTAX_ERROR_GISP_1 				= "mcss.type.message.050";
    public final static String TYPE_MSG_SYNTAX_ERROR_GISP_2 				= "mcss.type.message.051";
    public final static String TYPE_MSG_GISP_RESPONSE_NOT_OK 				= "mcss.type.message.052";
    public final static String TYPE_MSG_GISP_TOU_UNKNOWN 					= "mcss.type.message.053";
    public final static String TYPE_MSG_GISP21_NOT_AVAILABLE 				= "mcss.type.message.054";
    public final static String TYPE_MSG_TYPE_USER_NOT_ENABLED 				= "mcss.type.message.055";
    public final static String TYPE_MSG_USER_CF_NOT_ENABLE_FOR_SERVICE 		= "mcss.type.message.056";
    public final static String TYPE_MSG_USER_PRE_PRE_ACTIVATION 			= "mcss.type.message.057";
    public final static String TYPE_MSG_CUSTOMER_DISABLED_IN_NETWORK 		= "mcss.type.message.058";
    public final static String TYPE_MSG_CUSTOMER_BLOCKED_FOR_THEFT_LOSS 	= "mcss.type.message.059";
    public final static String TYPE_MSG_CUSTOMER_BLOCKED_FOR_ARREARS		= "mcss.type.message.060";
	public final static String TYPE_MSG_CUSTOMER_BLOCKED_FOR_FRAUD			= "mcss.type.message.061";
    public final static String TYPE_MSG_CUSTOMER_SMT_BLOCKED				= "mcss.type.message.062";
	public final static String TYPE_MSG_CUSTOMER_SMO_BLOCKED				= "mcss.type.message.063";
	public final static String TYPE_MSG_CUSTOMER_CARD_EXPIRED  				= "mcss.type.message.064";
    public final static String TYPE_MSG_CUSTOMER_RIF_CLOSED					= "mcss.type.message.065";
    public final static String TYPE_MSG_CUSTOMER_CARD_PREACTIVED  			= "mcss.type.message.066";
 	public final static String TYPE_MSG_CUSTOMER_CARD_CREDIT_ENDED   		= "mcss.type.message.067";
    public final static String TYPE_MSG_BILLING_NOT_ALLOWED			   		= "mcss.type.message.068";
    public final static String TYPE_MSG_BSCS_SYSTEM_FAULT			   		= "mcss.type.message.069";
    
    
    public final static String TYPE_MSG_ERR_RET_CODE_NOT_IN_HAPPY_HOUR		= "mcss.type.message.070";
    public final static String TYPE_MSG_ICARD_AUTHORIZATION_GRANTED	    	= "mcss.type.message.071";
    
    public final static String TYPE_MSG_SDL_SYSTEM_GENERIC_ERROR 			= "mcss.type.message.072";
    
    
    public final static String TYPE_MSG_PIN_AUTHORIZATION_GRANTED 			= "mcss.type.message.101";
    public final static String TYPE_MSG_PIN_GENERATED 						= "mcss.type.message.102";
    public final static String TYPE_MSG_PIN_BURNED 							= "mcss.type.message.103";
    
    public final static String TYPE_MSG_PIN_IS_MANDATORY 					= "mcss.type.message.104";
    public final static String TYPE_MSG_PIN_IS_INVALID 						= "mcss.type.message.105";
    public final static String TYPE_MSG_CMD_NOT_ALLOWED_FOR_PIN 			= "mcss.type.message.106";
    public final static String TYPE_MSG_PIN_HAS_EXPIRED 					= "mcss.type.message.107";
    public final static String TYPE_MSG_NO_ACTIVE_PIN						= "mcss.type.message.108";
    public final static String TYPE_MSG_PIN_FALED_TO_SEND					= "mcss.type.message.109";        

    public final static String TYPE_MSG_PARTIAL_BILLING_REQUREST_ACCEPTED	= "mcss.type.message.201";
    
    public final static String TYPE_MSG_TEMPORARY_UNKNOWN_USER				= "mcss.type.message.202";
    
    public final static String TYPE_MSG_SLA_EXCEEDED						= "mcss.type.message.301";

    public static final String PARAM_JCA = "jca.";
    public static final String PARAM_JCA_INITIAL_CONTEXT_FACTORY = "jca.initial.context.factory.";
    public static final String PARAM_JCA_PROVIDER_URL = "jca.provider.url.";
    public static final String PARAM_JCA_JNDI_NAME = "jca.jndi.name.";
    
    public static final String PARAM_PREPAID_CHKLANGID_FLAG = "mcss.prepaid.chklangid.flag";
    public static final String PARAM_PREPAID_BUD_LANGID_VALUE = "mcss.prepaid.bud.langid.value";
    public static final String PARAM_PREPAID_BLOCKED_CARD_CODE = "mcss.prepaid.blocked.card.code.";
    
	/*********************************************/
	/**Gestione Mascheramento Utenze nei Log	**/
	/*********************************************/
    public final static String PARAM_IS_USER_MASK_ENABLED  		  = "mcss.is.user.mask.enabled";
    public final static String PARAM_USER_MASK_FORMAT	  		  = "mcss.user.mask.format";
    
    /*  param jms bulk  - Inizio */
    public final static String PARAM_BULK_JMS_QUEUE_NUMBER			   	= "mcss.bulk.jms.queue.number";
    public final static String MSG_BULK_ERR_JMS_FAILURE					= "mcss.msg.err.jms.failure";

    public final static String PARAM_JMS_BULK_SERVER_FACTORY     = "mcss.jms.bulk.server.factory.";
    public final static String PARAM_JMS_BULK_SERVER_URL         = "mcss.jms.bulk.server.url.";
    public final static String PARAM_JMS_BULK_CONNECTION_FACTORY = "mcss.jms.bulk.connection.factory.";
    public final static String PARAM_JMS_BULK_QUEUE_NAME         = "mcss.jms.bulk.queue.name.";
    public final static String PARAM_MCSS_NODE_ID         		 = "mcss.node.id";
    public final static String PARAM_MCSS_BULK_FAILED_RESPONSE   = "mcss.bulk.failed.response";
    public final static String PARAM_BULK_CREATES_FATAL   		 = "mcss.bulk.creates.fatal";    
    
    public final static String PARAM_JMS_BULK_FATAL_SERVER_FACTORY 		= "mcss.jms.bulk.fatal.server.factory";
    public final static String PARAM_JMS_BULK_FATAL_SERVER_URL 			= "mcss.jms.bulk.fatal.server.url";
    public final static String PARAM_JMS_BULK_FATAL_CONNECTION_FACTORY 	= "mcss.jms.bulk.fatal.connection.factory";
    public final static String PARAM_JMS_BULK_FATAL_QUEUE_NAME 			= "mcss.jms.bulk.fatal.queue.name";
    
    public final static String PARAM_JMS_IMEI_PARSER_SERVER_FACTORY        = "mcss.jms.imei.parser.server.factory";
    public final static String PARAM_JMS_IMEI_PARSER_SERVER_URL			   = "mcss.jms.imei.parser.server.url";
    public final static String PARAM_JMS_IMEI_PARSER_CONNECTION_FACTORY	   = "mcss.jms.imei.parser.connection.factory";
    public final static String PARAM_JMS_IMEI_PARSER_QUEUE_NAME			   = "mcss.jms.imei.parser.queue.name";
    /*  param jms bulk - Fine */
    
    
    /**************************************
    /* REBILLING						 * /
    /*************************************/
    
    public final static String PARAM_JMS_REBILLING_SERVER_FACTORY       	= "mcss.jms.rebilling.server.factory";
    public final static String PARAM_JMS_REBILLING_SERVER_URL			   	= "mcss.jms.rebilling.server.url";
    public final static String PARAM_JMS_REBILLING_CONNECTION_FACTORY	   	= "mcss.jms.rebilling.connection.factory";
    public final static String PARAM_JMS_REBILLING_QUEUE_NAME			   	= "mcss.jms.rebilling.queue.name";
    public final static String PARAM_REBILLING_RETRY_LIMIT 					= "mcss.rebilling.retry.limit.";
    public final static String PARAM_REBILL_USE_ORACLE_SORT 				= "mcss.rebilling.use.oracle.sort";
    public final static String PARAM_USE_TH_ALLWAYS			 				= "mcss.use.th.allways";
    
    /*************************************/
    /* RETRY							 */
    /*************************************/
    
    public final static String PARAM_RETRY_CODES_ALWAYS				       	= "mcss.retry.codes.always";
    public final static String PARAM_RETRY_CODES_TIMER				       	= "mcss.retry.codes.timer";
    public final static String PARAM_RETRY_CODES_RELOAD			   			= "mcss.retry.codes.reload";
    public final static String PARAM_RETRY_CODES_PARTIAL				   	= "mcss.retry.codes.partial";
    public final static String PARAM_RETRY_MAX_DAYS_TO_LIVE				   	= "mcss.retry.max.days.to.live";
    public final static String PARAM_RETRY_MAX_DAILY_AMOUNT				   	= "mcss.retry.max.daily.amount";
    public final static String PARAM_RETRY_MAX_TOTAL_AMOUNT				   	= "mcss.retry.max.total.amount";
    public final static String PARAM_UNSUBSCRIBE_MAX_TO_LIVE				= "mcss.unsubscribe.max.to.live";
    public final static String PARAM_USE_SYCHRONIZED_PROCEDURE				= "mcss.use.synchronized.procedure";
    public final static String PARAM_USE_CREDITTOKKEP_ON_TH					= "mcss.use.credit.to.keep.on.th";

    /*************************************/
    /* SLA                               */
    /*************************************/
    
    public final static String PARAM_SLA_QUEUE_NAME					       	= "mcss.sla.queue.name";
    public final static String PARAM_SLA_CONN_FACTORY_NAME			       	= "mcss.sla.conn.factory.name";
    public final static String PARAM_SLA_PROVIDER_URL			   			= "mcss.sla.provider.url";
    
    /*********************************************/
	/**Gestione Valuta							**/
	/*********************************************/
    public final static String PARAM_HTTP_SERV_CHRG_TIPO_VAL   = "mcss.http.serv.chrg.tipo.val";
    public final static String PARAM_HTTP_SERV_CHRG_VALORE_VAL = "mcss.http.serv.chrg.valore.val";
    public final static String PARAM_HTTP_SERV_CHRG_VALORE_VAL_VALUES = "mcss.http.serv.chrg.tipo.val.values";
    
	public final static String MSG_TIPO_VAL_NOT_ALLOWED     = "mcss.message.074";
    public final static String TYPE_TIPO_VAL_NOT_ALLOWED 	= "mcss.type.message.000.074";
	public final static String MSG_VAL_VAL_NOT_ALLOWED      = "mcss.message.075";
    public final static String TYPE_VAL_VAL_NOT_ALLOWED 	= "mcss.type.message.000.075";
	public final static String MSG_VAL_VAL_MANDATORY        = "mcss.message.076";
    public final static String TYPE_VAL_VAL_MANDATORY 	    = "mcss.type.message.000.076";
	public final static String MSG_TIPO_VAL_MANDATORY       = "mcss.message.077";
    public final static String TYPE_TIPO_VAL_MANDATORY 	    = "mcss.type.message.000.077";
	public final static String MSG_CAMPI_VAL_MANADATORY     = "mcss.message.080";
    public final static String TYPE_CAMPI_VAL_MANADATORY  	= "mcss.type.message.000.080";
	public final static String MSG_GESTIONE_VAL_NOT_ALLOWED = "mcss.message.081";
    public final static String TYPE_GESTIONE_VAL_NOT_ALLOWED= "mcss.type.message.000.081";
    
    public final static String PARAM_MAX_VALUTA_VAT 	    = "mcss.max.valuta.vat";
    public final static String PARAM_MAX_VALUTA_NO_VAT 	    = "mcss.max.valuta.no.vat";
    public final static String PARAM_ENABLE_GESTIONE_VALUTA = "mcss.enable.gestione.valuta";
    public final static String PARAM_VAT_PERCENTAGE  	    = "mcss.vat.percentage";
	/*********************************************/

    /*********************************************/
	/**Gestione Transaction ID					**/
	/*********************************************/
    public final static String PARAM_HTTP_TRANS_ID   		  = "mcss.http.trans.id";
	public final static String MSG_TRANS_ID_NOT_ALLOWED       = "mcss.message.078";
    public final static String TYPE_TRANS_ID_NOT_ALLOWED 	  = "mcss.type.message.000.078";
	public final static String MSG_TRANS_ID_NOT_UNIQUE        = "mcss.message.079";
    public final static String TYPE_TRANS_ID_NOT_UNIQUE 	  = "mcss.type.message.000.079";
	public final static String MSG_TRANS_ID_ALREADY_BILLED    = "mcss.message.080";
    public final static String TYPE_TRANS_ID_ALREADY_BILLED   = "mcss.type.message.000.080";
	public final static String MSG_TRANS_ID_UNRELATED  		  = "mcss.message.081";
    public final static String TYPE_TRANS_ID_UNRELATED		  = "mcss.type.message.000.081";
    public final static String PARAM_TRANS_ID_RETRY_EXTENSION = "mcss.transid.retry.extension"; 
	/*********************************************/
        
    /*********************************************/
	/**WTC Integration      					**/
	/*********************************************/
    public final static String PARAM_FACTORY_TUXEDO_CONNECTION 		= "mcss.factory.tuxedo.connection"; 
    public final static String PARAM_SERVICE_NAME_POSTPAID_TUXEDO 	= "mcss.service.name.postpaid.tuxedo";
    
    public final static String PARAM_TUXEDO_POSTPAID_SUSPENDIDO 	= "mcss.tuxedo.postpaid.suspendido";
    public final static String PARAM_TUXEDO_POSTPAID_LINEA_GSM 		= "mcss.tuxedo.postpaid.linea.gsm";
    public final static String PARAM_TUXEDO_POSTPAID_NUM_CONTRATO 	= "mcss.tuxedo.postpaid.num.contrato";
    public final static String PARAM_TUXEDO_POSTPAID_CUSTOMER_ID 	= "mcss.tuxedo.postpaid.customer.id";
    public final static String PARAM_TUXEDO_POSTPAID_TIPO_LINEA		= "mcss.tuxedo.postpaid.tipo.linea";
    public final static String PARAM_TUXEDO_POSTPAID_ID_ERROR		= "mcss.tuxedo.postpaid.id.error";
    
    public final static String PARAM_TUXEDO_POSTPAID_TYPE_RETURN	= "mcss.tuxedo.postpaid.type.return";
    public final static String PARAM_TUXEDO_POSTPAID_ENABLED		= "mcss.tuxedo.postpaid.enabled";
    
    public final static String PARAM_TUXEDO_POSTPAID_SUSP_VAL_0		= "mcss.tuxedo.postpaid.susp.val.0";
    public final static String PARAM_TUXEDO_POSTPAID_SUSP_VAL_1		= "mcss.tuxedo.postpaid.susp.val.1";
    /*********************************************/
    /*
    SUSPENDIDO   short
    LINEA_GSM     short
    NUM_CONTRATO   long       
    CUSTOMER_ID long
    TIPO_LINEA string
    ID_ERROR
    */
    
    /*********************************************/
	/**MUTUA AUTENTICAZIONE CSP 				**/
	/*********************************************/
    public final static String PARAM_IS_SSL_DEBUG_ENABLED				= "mcss.is.ssl.debug.enabled";
    public final static String PARAM_IS_MEMORY_CERTIFICATE_ENABLED		= "mcss.is.memory.certificate.enabled";
    public final static String PARAM_SSL_SPEC_VERSION					= "mcss.ssl.spec.version";
    public final static String PARAM_CSP_CREDENTIAL_REQUIRED			= "mcss.csp.credential.required";
    public final static String PARAM_IS_CSP_CREDENTIAL_MASKED			= "mcss.is.csp.credential.masked";
    
    public Parameters() {
        System.out.println("Parameters - Begin");
        this.params = new Hashtable();
        this.L4JDef = new Properties();

		/*
		 * loadParameters();
		 */
		System.out.println("Parameters - Setting Logger Defaults");
		loadL4jDefault();
		PropertyConfigurator.configure(L4JDef);
		System.out.println("Parameters - Logger Defaults OK");
		loadMessages();
		System.out.println("Parameters - End");
    }

    private void loadL4jDefault() {
        // log4j
        L4JDef.put(PARAM_LOG4J_ROOTLOGGER, "DEBUG, stdout");
        L4JDef.put(PARAM_LOG4J_APPENDER_CONSOLE, "org.apache.log4j.ConsoleAppender");
        L4JDef.put(PARAM_LOG4J_APPENDER_CONSOLE_LAYOUT, "org.apache.log4j.PatternLayout");
        L4JDef.put(PARAM_LOG4J_APPENDER_CONSOLE_LAYOUTPATTERN, "MCSS %d{ISO8601} [%p] [%c] : [%m]%n");
        L4JDef.put(PARAM_LOG4J_APPENDER_FILE, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_FILE_NAME, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_FILE_LAYOUT, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_FILE_LAYOUTPATTERN, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_ROLLINGFILE, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_NAME, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUT, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUTPATTERN, "");
        L4JDef.put(PARAM_LOG4J_BULKLOGGER, "INFO, bulkfile");
        L4JDef.put(PARAM_LOG4J_ADDITIVITY_BULKLOGGER, "false");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKFILE, "org.apache.log4j.ConsoleAppender");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKFILE_NAME, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKFILE_LAYOUT, "org.apache.log4j.PatternLayout");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKFILE_LAYOUTPATTERN, "MCSS.BULK %d{ISO8601} [%p] [%c] : [%m]%n");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_NAME, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUT, "");
        L4JDef.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUTPATTERN, "");
        L4JDef.put(PARAM_LOG4J_MCSS_GROUP, "mcss");
        L4JDef.put(PARAM_LOG4J_CONTROLLER_GROUP, "mcss.controller");
        L4JDef.put(PARAM_LOG4J_CATALOG_GROUP, "mcss.catalog");
        L4JDef.put(PARAM_LOG4J_AUTHORIZE_GROUP, "mcss.authorize");
        L4JDef.put(PARAM_LOG4J_BILLING_GROUP, "mcss.billing");
        L4JDef.put(PARAM_LOG4J_CHECKTID_GROUP, "mcss.checktid");
        L4JDef.put(PARAM_LOG4J_OPSC_GROUP, "mcss.opsc");
        L4JDef.put(PARAM_LOG4J_BULK_GROUP, "bulkLogger");
    }

    /** Caricamento dei messaggi dell'applicazione. */
    private void loadMessages() {
        params.put(MSG_AUTHORIZATION_GRANTED, "Autorizzazione concessa.");
        params.put(MSG_BILLING_REQUEST_ACCEPTED, "Richiesta di addebito acquisita.");
        params.put(MSG_CHECKTID_REQUEST_ACCEPTED, "Richiesta check TID erogata.");
        params.put(MSG_ROLLBACK_EXECUTED, "Rollback Eseguito.");
        params.put(MSG_RELOAD_OK, "Reload della Configurazione Eseguito.");
        params.put(MSG_USER_ENABLED, "Utente Abilitato.");
        params.put(MSG_RECYCLE_COMMAND_OK, "Richieste di Billing Riaccodate con Successo.");
        params.put(MSG_RECYCLESMSO_COMMAND_OK, "SMS OUTBOUND Riaccodati con Successo.");
        params.put(MSG_UNKNOWN_CSP, "CSP sconosciuto.");
        params.put(MSG_CSP_NOT_ENABLED, "CSP non abilitato.");
        params.put(MSG_UNKNOWN_USER, "Utenza non TIM.");
        params.put(MSG_UNKNOWN_SERVICE, "Servizio sconosciuto.");
        params.put(MSG_USER_NOT_ENABLED, "Utente non abilitato.");
        params.put(MSG_NOT_ENOUGH_CREDIT, "Credito non sufficiente.");
        params.put(MSG_SYSTEM_NOT_AVAILABLE, "Sistema non disponibile.");
        params.put(MSG_SERVICE_NOT_AVAILABLE_MISSING_AUTHORIZATION_CODE, "Servizio non erogabile.");
        params.put(MSG_SERVICE_NOT_AVAILABLE_COST_NOT_COMPUTED, "Servizio non erogabile.");
        params.put(MSG_SERVICE_NOT_AVAILABLE_TOO_MUCH_RECIPIENTS, "Servizio non erogabile.");
        params.put(MSG_AUTHORIZATION_CODE_ALREADY_USED, "Codice di autorizzazione gia' utilizzato.");
        params.put(MSG_AUTHORIZATION_NOT_VALID_FOR_RECIPIENT, "Codice non valido per il destinatario richiesto.");
        params.put(MSG_REQUEST_CONTENTS_NOT_VALID, "Richiesta non correttamente valorizzata.");
        params.put(MSG_AUTHORIZATION_CODE_NOT_VALID, "Codice di autorizzazione non valido.");
        params.put(MSG_EXPIRED_AUTHORIZATION_CODE, "Codice di autorizzazione scaduto.");
        params.put(MSG_CSP_IS_MANDATORY, "CSP obbligatorio.");
        params.put(MSG_CSP_USER_IS_MANDATORY, "Utente CSP obbligatorio.");
        params.put(MSG_CSP_PASSWORD_IS_MANDATORY, "Password CSP obbligatoria.");
        params.put(MSG_RECIPIENT_IS_MANDATORY, "Destinatario obbligatorio.");
        params.put(MSG_MSISDN_IS_MANDATORY, "Msisdn obbligatorio.");
        params.put(MSG_SERVICE_ID_IS_MANDATORY, "Service_Id obbligatorio.");
        params.put(MSG_SERVICE_CLASS_IS_MANDATORY, "Service_Class obbligatorio.");
        params.put(MSG_EVENT_ID_IS_MANDATORY, "Event_Id obbligatorio.");
        params.put(MSG_WRONG_REQUEST, "Richiesta errata.");
        params.put(MSG_UNKNOWN_FUNCTION, "Funzione sconosciuta.");
        params.put(MSG_UNITIM_SERVICE_NOT_AVAILABLE, "UniTim fuori servizio.");
        params.put(MSG_UNITIM_SERVICE_WRONG_MSISDN_FORMAT, "Formato Msisdn non valido per UniTim.");
        params.put(MSG_OPSC_SYSTEM_GENERIC_ERROR, "Sistema OPSC non disponibile.");
        params.put(MSG_TYPE_OF_CARD_NOT_QUALIFIED, "Tipo Carta non abilitata a Servizio Vas a Canone.");
        params.put(MSG_WRONG_CONFIGURATION_OF_CARD_VAS, "Errata configurazione sistema per Servizio Vas a Canone.");
        params.put(MSG_ERR_INFOBUS_ERROR, "Errore generico InfoBus");
        params.put(MSG_WARN_INFOBUS_NOT_AVAILABLE, "InfoBus momentaneamente non disponibile");
        params.put(MSG_NOT_POST_SERVICE, "Servizio non Abilitato per Utenti Abbonati");
        params.put(MSG_NOT_PRE_SERVICE, "Servizio non Abilitato per Utenti Prepagati");
        params.put(MSG_AUTHORIZATION_TID_NOT_UNIQUE, "Codice TID gia' utilizzato.");
        params.put(MSG_TID_IS_MANDATORY, "TID obbligatorio.");
        params.put(MSG_TID_CODE_NOT_VALID, "TID code non correttamente formattato.");
        params.put(MSG_TID_RECORD_NOT_FOUND, "Record di autorizzazione non trovato");
        params.put(MSG_WRONG_CONFIGURATION_TID, "Errata configurazione TID");
        params.put(MSG_CSP_NOT_ENABLED_FOR_TID, "CSP non abilitato alla richiesta TID");
        params.put(MSG_NO_ACTIVE_RESERVATION, "Nessuna Reservation Attiva");
        params.put(MSG_USER_IS_MOROSO, "Utenza in stato di blocco antifrode");
        params.put(MSG_RELOAD_NOT_ALLOWED, "Client non abilitato alla richiesta");
        params.put(MSG_RELOAD_FAILED, "Reload Fallito per uno o piu' Moduli");
        params.put(MSG_ERR_GISP_NOT_AVAILABLE, "Sistema GISP non disponibile");
        params.put(MSG_ERR_RET_CODE_GISP_UNKNOWN, "Ret Code Gisp generico");
        params.put(MSG_USER_IS_STOPPED, "Utenza cessata");
        params.put(MSG_USER_IS_BLOCKED, "Utenza bloccata");
        params.put(MSG_SYNTAX_ERROR_GISP_1, "Syntax Error su i/f GISP-1");
        params.put(MSG_SYNTAX_ERROR_GISP_2, "Sintax Error su i/f GISP opzione 2in1");
        params.put(MSG_GISP_RESPONSE_NOT_OK, "Eccezione su i/f GISP opzione 2in1");
        params.put(MSG_GISP_TOU_UNKNOWN, "Eccezione ToU sconosciuto");
        params.put(MSG_GISP21_NOT_AVAILABLE, "Sistema GISP opzione 2in1 non disponibile");
        params.put(MSG_TYPE_USER_NOT_ENABLED, "Utenza non abilitata al servizio");
        params.put(MSG_USER_CF_NOT_ENABLE_FOR_SERVICE, "Servizio non abilitato ad utenti non certificati");
		params.put(MSG_USER_PRE_PRE_ACTIVATION, "Servizio non abilitato ad utenti in stato di pre-preattivazione");
        params.put(MSG_CUSTOMER_DISABLED_IN_NETWORK, "Utenza prepagata non abilitata in rete");
        params.put(MSG_CUSTOMER_BLOCKED_FOR_THEFT_LOSS, "Utenza in stato di blocco per furto/smarrimento");
		params.put(MSG_CUSTOMER_BLOCKED_FOR_ARREARS, "Utenza in stato di blocco per morosit�");
		params.put(MSG_CUSTOMER_BLOCKED_FOR_FRAUD, "Utenza in stato di blocco per frode");
		params.put(MSG_CUSTOMER_SMT_BLOCKED, "Utenza instato di blocco per la direttice SMT");
        params.put(MSG_CUSTOMER_SMO_BLOCKED, "Utenza instato di blocco per la direttice SMO");
        params.put(MSG_CUSTOMER_CARD_EXPIRED, "Utenza prepagata in stato di fine validita'");
		params.put(MSG_CUSTOMER_RIF_CLOSED, "Utenza appartenente ad una RIF chiusa");
		params.put(MSG_CUSTOMER_CARD_PREACTIVED, "Utenza abbonata in stato di preattivazione");
		params.put(MSG_CUSTOMER_CARD_CREDIT_ENDED, "Utenza prepagata in stato di fine credito");
		params.put(MSG_BILLING_NOT_ALLOWED, "Billing non consentito");
		params.put(MSG_BSCS_SYSTEM_FAULT, "Sistema BSCS non disponibile");
		
		params.put(MSG_ERR_RET_CODE_NOT_IN_HAPPY_HOUR,"La richiesta non e' stata fatta nell'Happy Hour");
		params.put(MSG_ICARD_AUTHORIZATION_GRANTED,"Autorizzazione Icard concessa");
    }

    /** Caricamento dei parametri di default. */
    private void loadParameters() {
        // parametri di sistema
        params.put(PARAM_CONFIGURATION_REFRESH_TIME_MINUTES, "5"); // ritardo
																	// per il
																	// refresh
																	// del "CM"
																	// (in
																	// minuti)
        params.put(PARAM_USE_DAYLIGHT_SAVING_TIME, "no"); // valori possibili
															// yes/no
        params.put(PARAM_CURRENCY_FACTOR_MULTIPLIER_FOR_OPSC, "10000"); // credito
																		// opsc
																		// x
																		// factor
																		// =
																		// credito
																		// formato
																		// interno!!!
        // parametri richiesta HTTP
        params.put(PARAM_HTTP_FUNCTION, "FUN");
        params.put(PARAM_HTTP_AUTHORIZATIONCODE, "AUTHCODE");
        params.put(PARAM_HTTP_UVI, "UVI");
        params.put(PARAM_HTTP_MSISDN, "MSISDN");
        params.put(PARAM_HTTP_TOTRECIP, "TOTRECIP");
        params.put(PARAM_HTTP_RECIPIENT, "RECIP");
        params.put(PARAM_HTTP_SERVICEID1, "SER1");
        params.put(PARAM_HTTP_SERVICEID2, "SER2");
        params.put(PARAM_HTTP_SERVICEID3, "SER3");
        params.put(PARAM_HTTP_CSP, "CSP");
        params.put(PARAM_HTTP_USR, "USR");
        params.put(PARAM_HTTP_PWD, "PWD");
        params.put(PARAM_HTTP_VOLUME, "VOL");
        params.put(PARAM_HTTP_TIME, "TIME");
        params.put(PARAM_HTTP_CONTENT, "CONT");
        params.put(PARAM_HTTP_QUANTITY, "QT");
        params.put(PARAM_HTTP_STARTTIME, "STARTTIME");
        params.put(PARAM_HTTP_ENDTIME, "ENDTIME");
        params.put(PARAM_HTTP_SERVICE_NAME, "SERVICENAME");
        params.put(PARAM_HTTP_TID, "TID");
        params.put(PARAM_HTTP_CREDRES, "RESERVATION");
        params.put(PARAM_HTTP_SERVICEKEY, "SER");
		params.put(PARAM_HTTP_SEND_SMS, "SENDSMS");
		params.put(PARAM_HTTP_TEXT_SMS, "TEXTSMS");
		params.put(PARAM_HTTP_OADC_SMS, "OADCSMS");

		params.put(PARAM_HTTP_TEXT_UNICODE_SMS, "UFT-8");
		params.put(PARAM_HTTP_TEXT_UNICODE_BULK, "UFT-8");
        // Log4J
        params.put(PARAM_LOG4J_ROOTLOGGER, "DEBUG, stdout");
        params.put(PARAM_LOG4J_APPENDER_CONSOLE, "org.apache.log4j.ConsoleAppender");
        params.put(PARAM_LOG4J_APPENDER_CONSOLE_LAYOUT, "org.apache.log4j.PatternLayout");
        params.put(PARAM_LOG4J_APPENDER_CONSOLE_LAYOUTPATTERN, "MCSS %d{ISO8601} [%p] [%c] : [%m]%n");
        params.put(PARAM_LOG4J_APPENDER_FILE, "");
        params.put(PARAM_LOG4J_APPENDER_FILE_NAME, "");
        params.put(PARAM_LOG4J_APPENDER_FILE_LAYOUT, "");
        params.put(PARAM_LOG4J_APPENDER_FILE_LAYOUTPATTERN, "");
        params.put(PARAM_LOG4J_APPENDER_ROLLINGFILE, "");
        params.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_NAME, "");
        params.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUT, "");
        params.put(PARAM_LOG4J_APPENDER_ROLLINGFILE_LAYOUTPATTERN, "");
        params.put(PARAM_LOG4J_BULKLOGGER, "INFO, bulkfile");
        params.put(PARAM_LOG4J_ADDITIVITY_BULKLOGGER, "false");
        params.put(PARAM_LOG4J_APPENDER_BULKFILE, "org.apache.log4j.ConsoleAppender");
        params.put(PARAM_LOG4J_APPENDER_BULKFILE_NAME, "");
        params.put(PARAM_LOG4J_APPENDER_BULKFILE_LAYOUT, "org.apache.log4j.PatternLayout");
        params.put(PARAM_LOG4J_APPENDER_BULKFILE_LAYOUTPATTERN, "MCSS.BULK %d{ISO8601} [%p] [%c] : [%m]%n");
        params.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE, "");
        params.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_NAME, "");
        params.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUT, "");
        params.put(PARAM_LOG4J_APPENDER_BULKROLLINGFILE_LAYOUTPATTERN, "");
        params.put(PARAM_LOG4J_MCSS_GROUP, "mcss");
        params.put(PARAM_LOG4J_CONTROLLER_GROUP, "mcss.controller");
        params.put(PARAM_LOG4J_CATALOG_GROUP, "mcss.catalog");
        params.put(PARAM_LOG4J_AUTHORIZE_GROUP, "mcss.authorize");
        params.put(PARAM_LOG4J_BILLING_GROUP, "mcss.billing");
        params.put(PARAM_LOG4J_CHECKTID_GROUP, "mcss.checktid");
        params.put(PARAM_LOG4J_OPSC_GROUP, "mcss.opsc");
        params.put(PARAM_LOG4J_BULK_GROUP, "bulkLogger");
        // valore parametri richiesta HTTP
        params.put(PARAM_HTTP_FUNCTION_AUTHORIZE, "AUTH");
        params.put(PARAM_HTTP_FUNCTION_BILLING, "BILL");
        params.put(PARAM_HTTP_FUNCTION_CHECKTID, "CHECK");
        params.put(PARAM_HTTP_FUNCTION_RESROLLBACK, "RRB");
        params.put(PARAM_HTTP_FUNCTION_RESROLLBACK, "RELOAD");
        params.put(PARAM_HTTP_FUNCTION_CHECKUSER, "INFO");
        // error pages
        params.put(PARAM_ERRORPAGE, "/error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_REQUEST, "/req-error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_COMMAND, "/com-error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_AUTHORIZE, "/auth-error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_BILLING, "/bill-error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_CHECKTID, "/check-error.jsp");
        params.put(PARAM_ERRORPAGE_FOR_RESROLLBACK, "/resrollback-error.jsp");
        // response pages
        params.put(PARAM_RESPONSEPAGE_FOR_MCSS, "/response.jsp");
        params.put(PARAM_RESPONSEPAGE_FOR_AUTHORIZE, "/auth-response.jsp");
        params.put(PARAM_RESPONSEPAGE_FOR_BILLING, "/bill-response.jsp");
        params.put(PARAM_RESPONSEPAGE_FOR_CHECKTID, "/check-response.jsp");
        params.put(PARAM_RESPONSEPAGE_FOR_RESROLLBACK, "/resrollback-response.jsp");
        params.put(PARAM_RESPONSEPAGE_FOR_MONITOR_BULK, "/monitor-bulk.jsp");
        
        
        // jndi
        params.put(PARAM_INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        params.put(PARAM_INITIAL_CONTEXT_PROVIDER_URL, "t3://10.6.92.54:9443");
        // EJB: ConfigurationManager
        params.put(PARAM_EJB_CONFIGURATIONMANAGER_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_CONFIGURATIONMANAGER_JNDI, "mcss.ConfigurationManagerHome");
        params.put(PARAM_EJB_CONFIGURATIONMANAGER_HOME, "com.atosorigin.mcss.config.ConfigurationManagerHome");
        // EJB: AuthorizationService
        params.put(PARAM_EJB_AUTHORIZATIONSERVICE_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_AUTHORIZATIONSERVICE_JNDI, "mcss.AuthorizationServiceHome");
        params.put(PARAM_EJB_AUTHORIZATIONSERVICE_HOME, "com.atosorigin.mcss.authorize.AuthorizationServiceHome");
        // EJB: BillingService
        params.put(PARAM_EJB_BILLINGSERVICE_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_BILLINGSERVICE_JNDI, "mcss.BillingServiceBeanHome");
        params.put(PARAM_EJB_BILLINGSERVICE_HOME, "com.atosorigin.mcss.billing.BillingServiceBeanHome");
        // EJB: CheckTidService
        params.put(PARAM_EJB_CHECKTIDSERVICE_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_CHECKTIDSERVICE_JNDI, "mcss.CheckTidServiceBeanHome");
        params.put(PARAM_EJB_CHECKTIDSERVICE_HOME, "com.atosorigin.mcss.checktid.CheckTidServiceBeanHome");
        // EJB: Authorization
        params.put(PARAM_EJB_AUTHORIZATION_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_AUTHORIZATION_JNDI, "mcss.AuthorizationHome");
        params.put(PARAM_EJB_AUTHORIZATION_HOME, "com.atosorigin.mcss.authorize.AuthorizationHome");
        // EJB: ResRollBack
        params.put(PARAM_EJB_RESROLLBACK_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_RESROLLBACK_JNDI, "mcss.ResRollBackHome");
        params.put(PARAM_EJB_RESROLLBACK_HOME, "com.atosorigin.mcss.resrollback.ResRollBackHome");
        // Parameters for QueueLocator
        params.put(PARAM_JMS_RESERVATION_SERVER_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        params.put(PARAM_JMS_RESERVATION_SERVER_URL, "t3://naunx28:7221");
        params.put(PARAM_JMS_RESERVATION_CONNECTION_FACTORY, "mcss.connection.factory");
        params.put(PARAM_JMS_RESERVATION_QUEUE_NAME, "mcss.queue.reservation");
        
        /** ICARD/MCSS Integration EJB: ICARD Credit ReservationServices */
        params.put(PARAM_EJB_ICARD_CREDRES_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_ICARD_CREDRES_JNDI, "icard.OnDemandHome");
        params.put(PARAM_EJB_ICARD_CREDRES_HOME, "com.schlumbergersema.icard.ondemandservices.OnDemandHome");
        
        params.put(PARAM_ICARD_DEFAULT_ZERO_COST_SERVICE_CLASS, "999");
        params.put(PARAM_ICARD_DEFAULT_ZERO_COST_SERVICE_ID,  "99999");
        params.put(PARAM_ICARD_DEFAULT_ZERO_COST_EVENT_ID,    "99999");
        
        params.put(PARAM_ICARD_WALLET_TYPE,    "WT");
        params.put(PARAM_ICARD_HAPPY_HOUR_TYPE,    "HH");

        
        
        // EJB: INFOBUS/OPSC
        params.put(PARAM_OPSC_OK_MSG, "1");
        params.put(PARAM_OPSC_MSG_SET + 0, "1,Successfully Accomplished Task");
        params.put(PARAM_OPSC_MSG_SET + 1, "-11,ADB/TDB error");
        params.put(PARAM_OPSC_MSG_SET + 2, "-12,OPSC internal error");
        params.put(PARAM_OPSC_MSG_SET + 3, "-13,SNB not in ADB");
        params.put(PARAM_OPSC_MSG_SET + 4, "-14,Server unavailable resources to perform transaction");
        params.put(PARAM_OPSC_MSG_SET + 5, "-15,Server unavailable internal structures to perform transaction");
        params.put(PARAM_OPSC_MSG_SET + 6, "-16,Application/user not authorized to access transaction or system");
        params.put(PARAM_OPSC_MSG_SET + 7, "-17,Wrong user password");
        params.put(PARAM_OPSC_MSG_SET + 8, "-18,Server cannot handle subtransaction");
        params.put(PARAM_OPSC_MSG_SET + 9, "-19,TID already used in TDB");
        params.put(PARAM_OPSC_MSG_SET + 10, "-22,SNB already present in TDB");
        params.put(PARAM_OPSC_MSG_SET + 11, "-23,SNB already in ADB");
        params.put(PARAM_OPSC_MSG_SET + 12, "-24,Subscriber�s credit too low to allow operation");
        params.put(PARAM_OPSC_MSG_SET + 13, "-25,Time limit overran");
        params.put(PARAM_OPSC_MSG_SET + 14, "-26,Subscriber already owns a group");
        params.put(PARAM_OPSC_MSG_SET + 15, "-27,Subscriber does not own a group");
        params.put(PARAM_OPSC_MSG_SET + 16, "-28,Unable to lock subscriber s record in ADB");
        params.put(PARAM_OPSC_MSG_SET + 17, "-29,Invalid subscriber s State");
        params.put(PARAM_OPSC_MSG_SET + 18, "-30,Group already in GDB");
        params.put(PARAM_OPSC_MSG_SET + 19, "-31,Group not in GDB");
        params.put(PARAM_OPSC_MSG_SET + 20, "-32,GDB error");
        params.put(PARAM_OPSC_MSG_SET + 21, "-33,Unable to lock group record in GDB");
        params.put(PARAM_OPSC_MSG_SET + 22, "-34,No Description");
        params.put(PARAM_OPSC_MSG_SET + 23, "-35,No Description");
        params.put(PARAM_OPSC_MSG_SET + 24, "-36,No Description");
        params.put(PARAM_OPSC_MSG_SET + 25, "-37,No Description");
        params.put(PARAM_OPSC_MSG_SET + 26, "-10,No Description");
        params.put(PARAM_OPSC_MSG_SET + 27, "-101,Message format syntax error");
        params.put(PARAM_OPSC_MSG_SET + 28, "-200,Message format syntax error");
        params.put(PARAM_OPSC_MSG_SET + 29, "-201,Wrong application header format");
        params.put(PARAM_OPSC_MSG_SET + 30, "-202,Wrong message length");
        params.put(PARAM_OPSC_MSG_SET + 31, "-203,Server instance does not manage the specified message type");
        // params.put(PARAM_OPSC_MSG_SET+31, "??,Network response code");
        // params.put(PARAM_OPSC_MSG_SET+32, "??,Network response code");

        // EJB: INFOBUS/GISP Returns
        // 0 protocol TAB SEPARATED
		// 1 protocol XML
        params.put(PARAM_GISP_PROTOCOL, "0");
        params.put(PARAM_GISP_FULL_OK_MSG, "00");
        params.put(PARAM_GISP_FULL_ABSENT_MSG, "10");
        params.put(PARAM_GISP_FULL_ERROR_MSG, "11");
        params.put(PARAM_GISP_FULL_MSG_SET + 0, "00,Utenza Presente");
        params.put(PARAM_GISP_FULL_MSG_SET + 1, "10,Utenza Non Presente");
        params.put(PARAM_GISP_VIRT_OK_MSG, "00");
        params.put(PARAM_GISP_VIRT_ABSENT_MSG, "10");
        params.put(PARAM_GISP_VIRT_ERROR_MSG, "11");
        params.put(PARAM_GISP_VIRT_MSG_SET + 0, "00,Utenza Virtual");
        params.put(PARAM_GISP_VIRT_MSG_SET + 1, "10,Utenza Non Virtual");
        params.put(PARAM_IBUS_ID_COMMIT, "COMMITBAL");
        params.put(PARAM_IBUS_NUM_COMMIT_RETRY, "10");
        params.put(PARAM_IBUS_APPDEP_1, "2");
        params.put(PARAM_IBUS_APPDEP_1_CREDIT, "0");
        params.put(PARAM_IBUS_COMMIT_BODY, "0,0,0,1");
        params.put(PARAM_EJB_IBUS_GW_URL, "t3://10.6.92.54:9443");
        params.put(PARAM_EJB_IBUS_GW_JNDI, "IBusGwBean");
        params.put(PARAM_EJB_IBUS_GW_HOME, "com.atosorigin.mcss.infobusgateway.IBusGwHome");
        params.put(PARAM_IBUS_ID_SYS, "MCSS");
        params.put(PARAM_OPSC_ID_SERV, "BALANCE");
        params.put(PARAM_OPSC_ID_SYSTEM, "MCSS");
        params.put(PARAM_GISP_STAT_SERV, "RETRSTATMCSS");
        params.put(PARAM_GISP_VIR_SERV, "RETRVIRMCSS");
        params.put(PARAM_GISP_ID_SYSTEM, "MCSS");
        params.put(PARAM_GISP_CAS_NAME_PAR, "CAS");
        params.put(PARAM_GISP_CAS_NAME_VAL, "MCSS");
        params.put(PARAM_GISP_REQUEST_NAME_PAR, "NOME_RICHIESTA");
        params.put(PARAM_GISP_REQUEST_NAME_FULL_VAL, "RETRSTATMCSS");
        params.put(PARAM_GISP_REQUEST_NAME_VIRT_VAL, "RETRVIRMCSS");
        params.put(PARAM_GISP_NUM_TEL_PAR, "NUM_TEL");
        // Type user
        params.put(PARAM_TYPE_PP, "PP");
        params.put(PARAM_TYPE_USER_2IN1A, "2in1A");
        params.put(PARAM_TYPE_USER_2IN1B, "2in1B");
        params.put(PARAM_TYPE_USER_PP2IN1A, "PP2in1A");
        params.put(PARAM_TYPE_USER_PP2IN1B, "PP2in1A");
        params.put(PARAM_TYPE_USER_ABB2IN1A, "ABB2in1A");
        params.put(PARAM_TYPE_USER_ABB2IN1B, "ABB2in1B");
        params.put(PARAM_TYPE_DEFABB, "Def_ABB");
        params.put(PARAM_TYPE_USER_ABBTFA, "ABBT_FA");
        params.put(PARAM_TYPE_USER_ABBTBU, "ABBT_BU");
        params.put(PARAM_TYPE_USER_ABBTMB,"ABBT_MB");
        params.put(PARAM_TYPE_USER_ABBMB, "ABB_MB");
        params.put(PARAM_TYPE_USER_ABBCO, "ABB_CO");
        params.put(PARAM_TYPE_USER_ABBBU, "ABB_BU");
		params.put(PARAM_TYPE_USER_UMTSMB,"UMTS_MB");
		params.put(PARAM_TYPE_USER_UMTSCO,"UMTS_CO");
		params.put(PARAM_TYPE_USER_UMTSBU,"UMTS_BU");

        params.put(PARAM_TYPE_USER_AOM, "AOM");
        // EJB: Infobus-J2EE provided by TIM
//        params.put(PARAM_EJB_EXTERNAL_INFOBUS_URL, "t3://naunx28:7219 t3://naunx28:7217");
//        params.put(PARAM_EJB_EXTERNAL_INFOBUS_JNDI, "ToInfoBUS");
//        params.put(PARAM_EJB_EXTERNAL_INFOBUS_HOME, "infobus.ejb.InfoBUSinHome");

        // opsc
//        params.put(PARAM_SOCKETADAPTER_LOCAL_ADDRESS, "10.6.92.26");
//        params.put(PARAM_SOCKETADAPTER_ADDRESSES, "0:10.41.27.61;1:10.41.27.63;2:10.41.27.106;3:10.41.27.101;4:10.41.27.72;5:10.41.27.123;6:10.41.27.60");
//        params.put(PARAM_SOCKETADAPTER_PORTS, "0:4018;1:4018;2:4018;3:4018;4:4018;5:4018;6:4018");
//        params.put(PARAM_OPSC_PATTERNS, "0:\\A338[13579];1:\\A338[2468];2:\\A339[2468];3:\\A339[13579];4:\\A333[2468];5:\\A333[13579];6:\\A(360|368|330|334|335|336|337)");
//        params.put(PARAM_OPSC_IDS, "0:100;1:200;2:300;3:400;4:500;5:600;6:700");
//        params.put(PARAM_OPSC_RESPONSE_TIMEOUT, "10");
//        params.put(PARAM_OPSC_CONNECT_WAIT, "5");
//        params.put(PARAM_OPSC_DEFAULT_BASKET, "0");
//        params.put(PARAM_OPSC_PROTOCOL, "0");
        params.put(PARAM_OPSC_NET_STATE_BLOCK_VALUE, "9");
//        params.put(PARAM_OPSC_XML_TAG_STATE_BLOCK, "RSANAG");
        // Hot Reload
        params.put(PARAM_RELOAD_SKIP_CHECK_IP, "N");
        params.put(PARAM_RELOAD_TRUSTED_IP, "localhost");
        params.put(PARAM_RELOAD_ALL_COMPONENTS, "Y");
        // bulk loader...
        params.put(PARAM_SKIP_CHECK_IP, "N");
        params.put(PARAM_BULK_LOADER_TRUSTED_ID, "t3://10.6.92.54:9443");
        // ID del sistema (IP:PORTA) sul quale viene eseguito il modulo di
		// caricamento dei file...
        params.put(PARAM_BULK_LOADER_ROOT_PATH, "bulkload");
        params.put(PARAM_BULK_LOADER_INPUT_DIRECTORY, "IN");
        params.put(PARAM_BULK_LOADER_BACKUP_DIRECTORY, "BKP");
        params.put(PARAM_BULK_LOADER_FAILED_DIRECTORY, "FAILED");
        params.put(PARAM_BULK_LOADER_FATAL_DIRECTORY, "FATAL");
        params.put(PARAM_BULK_FOLDER_ADD_PATH, "S"); // salvare il path con
														// il nome file
        params.put(PARAM_BULK_FOLDER_SLEEP_SECONDS, "15"); // attesa del thread
        params.put(PARAM_BULK_FOLDER_TABLENAME, "VISTA_DIRECTORY");
        params.put(PARAM_BULK_FOLDER_TABLEFIELD_LOADERID, "IP_DNS");
        params.put(PARAM_BULK_FOLDER_TABLEFIELD_TYPE, "TIPO_DIR");
        params.put(PARAM_BULK_FOLDER_TABLEFIELD_NAME, "NOME_FILE");
        params.put(PARAM_BULK_MAXRETRY, "5");
        params.put(PARAM_BULK_MAXRESULTSET, "10");
        params.put(PARAM_BULK_TIMEOUT_TRANSACTION, "240");
        params.put(PARAM_BULK_BLOCKSIZE, "1200");
        params.put(PARAM_BULK_READER_SLEEP_SECONDS, "15");
        params.put(PARAM_BULK_PROCESSOR_SLEEP_SECONDS, "15");
        params.put(PARAM_BULK_READER_PREFIX_FILENAME, "MCSS_");
		params.put(PARAM_BULK_READER_EXTENSION_FILENAME, ".new");
        // servlet esterne...
        params.put(PARAM_UNITIM_URL, "http://10.6.28.40:7011/InfoUniTim");
        params.put(PARAM_UNITIM_TIMEOUT, "5");
        params.put(PARAM_UNITIM_NUMTEL, "numtel");
        params.put(PARAM_UNITIM_CUSTCODE, "custcode");
        params.put(PARAM_UVITOMSISDN_URL, "http://10.6.28.39:80/igw-cgibin/GetMSISDNFromUVI");
        params.put(PARAM_UVITOMSISDN_UVI, "uvi");
        // Parametri UDR
        params.put(PARAM_SERV_CHARGE_TYPE_VOLUME, "0");
        params.put(PARAM_CHANNEL_TYPE_PTP, "5");
        params.put(PARAM_CHANNEL_TYPE_TRIGGER, "2");
        params.put(PARAM_USERHPLMNIDENTIFIER_PTP, "22201");
        params.put(PARAM_USERHPLMNIDENTIFIER_TRIGGER, "00000");
		params.put(PARAM_RECIPIENTHPLMNIDENTIFIER_TRIGGER, "00001");
        params.put(PARAM_MAX_BYTES_VOLUME, "2147483647");
        // Inizio gestione tipologia Messaggi
        params.put(TYPE_MSG_AUTHORIZATION_GRANTED, "S");
        params.put(TYPE_MSG_BILLING_REQUEST_ACCEPTED, "S");
        params.put(TYPE_MSG_CHECKTID_REQUEST_ACCEPTED, "S");
        params.put(TYPE_MSG_ROLLBACK_EXECUTED, "S");
        params.put(TYPE_MSG_RELOAD_OK, "S");
        params.put(TYPE_MSG_USER_ENABLED, "S");
        params.put(TYPE_MSG_UNKNOWN_CSP, "P");
        params.put(TYPE_MSG_CSP_NOT_ENABLED, "P");
        params.put(TYPE_MSG_UNKNOWN_USER, "P");
        params.put(TYPE_MSG_UNKNOWN_SERVICE, "P");
        params.put(TYPE_MSG_USER_NOT_ENABLED, "P");
        params.put(TYPE_MSG_NOT_ENOUGH_CREDIT, "P");
        params.put(TYPE_MSG_SYSTEM_NOT_AVAILABLE, "T");
        params.put(TYPE_MSG_SERVICE_MISSING_AUTHORIZATION_CODE, "P");
        params.put(TYPE_MSG_SERVICE_NOT_AVAILABLE_COST_NOT_COMPUTED, "P");
        params.put(TYPE_MSG_SERVICE_NOT_AVAILABLE_TOO_MUCH_RECIPIENTS, "P");
        params.put(TYPE_MSG_AUTHORIZATION_CODE_ALREADY_USED, "P");
        params.put(TYPE_MSG_AUTHORIZATION_NOT_VALID_FOR_RECIPIENT, "P");
        params.put(TYPE_MSG_REQUEST_CONTENTS_NOT_VALID, "P");
        params.put(TYPE_MSG_AUTHORIZATION_CODE_NOT_VALID, "P");
        params.put(TYPE_MSG_EXPIRED_AUTHORIZATION_CODE, "P");
        params.put(TYPE_MSG_CSP_IS_MANDATORY, "P");
        params.put(TYPE_MSG_CSP_USER_IS_MANDATORY, "P");
        params.put(TYPE_MSG_CSP_PASSWORD_IS_MANDATORY, "P");
        params.put(TYPE_MSG_RECIPIENT_IS_MANDATORY, "P");
        params.put(TYPE_MSG_MSISDN_IS_MANDATORY, "P");
        params.put(TYPE_MSG_SERVICE_ID_IS_MANDATORY, "P");
        params.put(TYPE_MSG_SERVICE_CLASS_IS_MANDATORY, "P");
        params.put(TYPE_MSG_EVENT_ID_IS_MANDATORY, "P");
        params.put(TYPE_MSG_WRONG_REQUEST, "P");
        params.put(TYPE_MSG_UNKNOWN_FUNCTION, "P");
        params.put(TYPE_MSG_UNITIM_SERVICE_NOT_AVAILABLE, "T");
        params.put(TYPE_MSG_UNITIM_SERVICE_WRONG_MSISDN_FORMAT, "P");
        params.put(TYPE_MSG_OPSC_SYSTEM_GENERIC_ERROR, "T");
        params.put(TYPE_MSG_TYPE_OF_CARD_NOT_QUALIFIED, "P");
        params.put(TYPE_MSG_WRONG_CONFIGURATION_OF_CARD_VAS, "P");
        params.put(TYPE_MSG_ERR_INFOBUS_ERROR, "T");
        params.put(TYPE_MSG_WARN_INFOBUS_NOT_AVAILABLE, "T");
        params.put(TYPE_MSG_NOT_POST_SERVICE, "P");
        params.put(TYPE_MSG_NOT_PRE_SERVICE, "P");
        params.put(TYPE_MSG_AUTHORIZATION_TID_NOT_UNIQUE, "P");
        params.put(TYPE_MSG_TID_IS_MANDATORY, "P");
        params.put(TYPE_MSG_TID_CODE_NOT_VALID, "P");
        params.put(TYPE_MSG_TID_RECORD_NOT_FOUND, "P");
        params.put(TYPE_MSG_WRONG_CONFIGURATION_TID, "P");
        params.put(TYPE_MSG_CSP_NOT_ENABLED_FOR_TID, "P");
        params.put(TYPE_MSG_NO_ACTIVE_RESERVATION, "P");
        params.put(TYPE_MSG_USER_IS_MOROSO, "P");
        params.put(TYPE_MSG_RELOAD_NOT_ALLOWED, "P");
        params.put(TYPE_MSG_RELOAD_FAILED, "P");
        params.put(TYPE_MSG_ERR_GISP_NOT_AVAILABLE, "T");
        params.put(TYPE_MSG_ERR_RET_CODE_GISP_UNKNOWN, "P");
        params.put(TYPE_MSG_USER_IS_STOPPED, "P");
        params.put(TYPE_MSG_USER_IS_BLOCKED, "P");
        params.put(TYPE_MSG_SYNTAX_ERROR_GISP_1, "T");
        params.put(TYPE_MSG_SYNTAX_ERROR_GISP_2, "T");
        params.put(TYPE_MSG_GISP_RESPONSE_NOT_OK, "T");
        params.put(TYPE_MSG_GISP_TOU_UNKNOWN, "P");
        params.put(TYPE_MSG_GISP21_NOT_AVAILABLE, "T");
        params.put(TYPE_MSG_TYPE_USER_NOT_ENABLED, "P");
        params.put(TYPE_MSG_USER_CF_NOT_ENABLE_FOR_SERVICE, "P");
		params.put(TYPE_MSG_USER_PRE_PRE_ACTIVATION, "P");
        params.put(TYPE_MSG_CUSTOMER_DISABLED_IN_NETWORK, "T");
		params.put(TYPE_MSG_CUSTOMER_BLOCKED_FOR_THEFT_LOSS, "P");
		params.put(TYPE_MSG_CUSTOMER_BLOCKED_FOR_ARREARS, "P");
		params.put(TYPE_MSG_CUSTOMER_BLOCKED_FOR_FRAUD, "P");
		params.put(TYPE_MSG_CUSTOMER_SMT_BLOCKED, "P");
		params.put(TYPE_MSG_CUSTOMER_SMO_BLOCKED, "P");
		params.put(TYPE_MSG_CUSTOMER_CARD_EXPIRED, "P");
		params.put(TYPE_MSG_CUSTOMER_RIF_CLOSED, "P");
		params.put(TYPE_MSG_CUSTOMER_CARD_PREACTIVED, "P");
		params.put(TYPE_MSG_CUSTOMER_CARD_CREDIT_ENDED, "P");
        params.put(TYPE_MSG_BILLING_NOT_ALLOWED, "P");
        
        params.put(TYPE_MSG_ERR_RET_CODE_NOT_IN_HAPPY_HOUR,"P");
        params.put(TYPE_MSG_ICARD_AUTHORIZATION_GRANTED,"S");
        // Fine gestione tipologia Messaggi

        // Inizio configurazione valori di default per statistiche..
        params.put(PARAM_STAT_SEPARATOR_ONE,"!");
        params.put(PARAM_STAT_SEPARATOR_TWO,";");
        params.put(PARAM_STAT_KEY_NOT_VALIDATED,"NOT_VAL");

        params.put(PARAM_STAT_CHECK_AUTH_IN,"Y");
        params.put(PARAM_STAT_KEY_INPUT_NOT_OK_AUTH,"InputNotOkAUTH");
        params.put(PARAM_STAT_KEY_INPUT_OK_AUTH,"InputOkAUTH");
        params.put(PARAM_STAT_KEY_OUT_AUTH_NOT_OK,"OutAuthNotOk");
        params.put(PARAM_STAT_KEY_OUT_AUTH_OK,"OutAuthOk");
        params.put(PARAM_STAT_KEY_OUT_RES_BALANCE_OUT,"OutResBALANCEOut");
        params.put(PARAM_STAT_KEY_OUT_RES_BALANCE_COMMIT,"OutResBALANCECOMMIT");
        params.put(PARAM_STAT_KEY_OUT_RES_BALANCE_OK,"OutResBALANCEOK");
        params.put(PARAM_STAT_KEY_OUT_RES_BALANCE_KO,"OutResBALANCEKO");
        params.put(PARAM_STAT_KEY_OUT_RES_BALANCE_TIMEOUT,"OutResBALANCETimeOut");
        params.put(PARAM_STAT_KEY_OUT_AUTH_ICARD_OUT,"OutAuthICARDOut");
        params.put(PARAM_STAT_KEY_OUT_AUTH_ICARD_KO,"OutAuthICARDKO");
        params.put(PARAM_STAT_KEY_OUT_AUTH_ICARD_OK,"OutAuthICARDOK");
        params.put(PARAM_STAT_KEY_INPUT_NOT_OK_BILL,"InputNotOkBILL");
        params.put(PARAM_STAT_KEY_INPUT_NOT_OK_BULK_BILL,"InputNotOkBULKBILL");
        params.put(PARAM_STAT_KEY_INPUT_OK_BILL,"InputOkBILL");
        params.put(PARAM_STAT_KEY_INPUT_OK_BULK_BILL,"InputOkBULKBILL");
        params.put(PARAM_STAT_KEY_OUT_BILL_NOT_OK,"OutBillNotOk");
        params.put(PARAM_STAT_KEY_OUT_BILL_OK_UDR,"OutBillOkUDR");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_OUT,"OutBillBALANCEOut");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_COMMIT,"OutBillBALANCECOMMIT");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_OK,"OutBillBALANCEOK");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_KO,"OutBillBALANCEKO");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_TIMEOUT,"OutBillBALANCETimeOut");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_QUEUE,"OutBillBALANCEQUEUE");
        params.put(PARAM_STAT_KEY_OUT_BILL_BULK_NOT_OK,"OutBillBULKNotOk");
        params.put(PARAM_STAT_KEY_OUT_BILL_OK_BULK_UDR,"OutBillOkBULKUDR");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_OUT,"OutBillBALANCEBULKOut");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_COMMIT,"OutBillBALANCEBULKCOMMIT");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_OK,"OutBillBALANCEBULKOK");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_KO,"OutBillBALANCEBULKKO");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_TIMEOUT,"OutBillBALANCEBULKTimeOut");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_QUEUE,"OutBillBALANCEBULKQUEUE");
        params.put(PARAM_STAT_KEY_OUT_BILL_SCARTO_BULK_NOT_OK,"OutBillSCARTOBULKNotOk");
        params.put(PARAM_STAT_KEY_OUT_BILL_SCARTO_BULK_NOT_OK,"OutBillSCARTOFATALBULKNotOk");
        params.put(PARAM_STAT_KEY_OUT_BILL_SCARTO_FATAL_BULK_NOT_OK,"OutBillSCARTOBULKFATAL");
        params.put(PARAM_STAT_KEY_OUT_BILL_BULK_RESPONSE_NOT_OK,"OutBillBULKRESPONSENotOk");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_PLUS_UDR_OK,"OutBillBALANCEPLUSUDROK");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_MINUS_UDR_OK,"OutBillBALANCEMINUSUDROK");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_PLUS_UDR_OK,"OutBillBALANCEBULKPLUSUDROK");
        params.put(PARAM_STAT_KEY_OUT_BILL_BALANCE_BULK_MINUS_UDR_OK,"OutBillBALANCEBULKMINUSUDROK");
        params.put(PARAM_STAT_KEY_OUT_ASN1_OK,"OutASN1Ok");
        params.put(PARAM_STAT_KEY_OUT_ASN1_FILE_NAME,"OutASN1FileName");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_OUT,"OutOnMessResBALANCEOut");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_COMMIT,"OutOnMessResBALANCECOMMIT");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_OK,"OutOnMessResBALANCEOK");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_BILLED_OK,"OutOnMessResBILLEDOK");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_KO,"OutOnMessResBALANCEKO");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_TIMEOUT,"OutOnMessResBALANCETimeOut");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BALANCE_QUEUE,"OutOnMessResBALANCEQUEUE");
        params.put(PARAM_STAT_KEY_OUT_ON_MESS_RES_BILL_BALANCE_ACTIVE,"OutOnMessResBillBALANCEActive");
        params.put(PARAM_STAT_KEY_OUT_BILL_ICARD_OUT,"OutBillICARDOut");
        params.put(PARAM_STAT_KEY_OUT_BILL_ICARD_KO,"OutBillICARDKO");
        params.put(PARAM_STAT_KEY_OUT_BILL_ICARD_OK,"OutBillICARDOK");
        params.put(PARAM_STAT_KEY_OUT_BILL_OK_ICARD_CH,"OutBillOKIcardCh");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_NOT_OK_AUTH, "InputParameterNotOkAUTH");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_NOT_OK_BILL, "InputParameterNotOkBILL");
        params.put(PARAM_STAT_KEY_INPUT_BILLING_WITH_AUTH,"WithAuth");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_GISP_FULL, "GispFullInterface");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_GISP_21, "Gisp21Interface");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_UNITIM, "UnitimInterface");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_UVI, "UviInterface");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_OPSC, "IncallOpscInterface");
        params.put(PARAM_STAT_KEY_INPUT_PARAMETER_SMS_CARING, "SmsCaringInterface");
        params.put(PARAM_STAT_KEY_RETRY_NOT_OK_BULK_BILL,"RetryNotOkBulkBILL");
        params.put(PARAM_STAT_KEY_RETRY_OK_BULK_BILL,"RetryOkBulkBILL");
        params.put(PARAM_STAT_KEY_RETRY_BILL_NOT_OK,"RETRYBillNotOk");

        params.put(PARAM_STAT_KEY_TYPE_OPERATION_DEC,"DECREMENTO");
        params.put(PARAM_STAT_KEY_TYPE_OPERATION_ADD,"ADDEBITO");
        params.put(PARAM_STAT_KEY_CHANGE_JMS_REDELIVERED,"Y");
        // Fine configurazione valori di default per statistiche..
        
        // Inizio valori default WTC Tuxedo postpaid
        params.put(PARAM_FACTORY_TUXEDO_CONNECTION,"tuxedo.services.TuxedoConnection");
        params.put(PARAM_SERVICE_NAME_POSTPAID_TUXEDO,"getInfoContrato");
        params.put(PARAM_TUXEDO_POSTPAID_SUSPENDIDO,"SUSPENDIDO");
        params.put(PARAM_TUXEDO_POSTPAID_LINEA_GSM,"LINEA_GSM");
        params.put(PARAM_TUXEDO_POSTPAID_NUM_CONTRATO,"NUM_CONTRATO");
        params.put(PARAM_TUXEDO_POSTPAID_CUSTOMER_ID,"CUSTOMER_ID");
        params.put(PARAM_TUXEDO_POSTPAID_TIPO_LINEA,"TIPO_LINEA");
        params.put(PARAM_TUXEDO_POSTPAID_ID_ERROR,"ID_ERROR");
        params.put(PARAM_TUXEDO_POSTPAID_TYPE_RETURN,"FML");
        params.put(PARAM_TUXEDO_POSTPAID_SUSP_VAL_1,"1");
        params.put(PARAM_TUXEDO_POSTPAID_SUSP_VAL_0,"0");
        // Fine valori default WTC Tuxedo postpaid
        
        
        params.put(PARAM_HTTP_FUNCTION_BULKMONITOR,"MONITOR");
        
        
    }

    /**
	 * Aggiorna i parametri dell'applicazione. Se il parametro da aggiornare non
	 * esiste, non esegue nulla.
	 */
    protected void setParameter(String key, String value) {
        // if ( params.containsKey(key) && value!=null && !value.equals("") ){
        params.put(key, value);
        // }
    }

    /** Restituisce la lista dei nomi dei parametri di configurazione. */
    public Enumeration getKeys() {
        return params.keys();
    }

    /**
	 * Restituisce il valore del parametro inidividuato dalla chiave key.
	 * Restituisce null se la chiave non esiste.
	 */
    public String getValue(String key) {
        if (params.containsKey(key)) {
            return (String)params.get(key);
        } else {
            return "";
        }
    }

    /** Restituisce il msg relativo alla key fornita. */
    public String getMessage(String key) {
        String msg = "Errore sconosciuto.";
        if (params.containsKey(key)) {
            msg = (String)params.get(key);
        }
        return msg;
    }

    public String getTypeMessage(String key) {
        // Meglio inizializzare ad errore permanente!!!
        String msg = "P";
        if (key.startsWith("mcss.message.")) {
            key = "mcss.type.message." + key.substring(13, key.length());
        }
        if (params.containsKey(key)) {
            msg = (String)params.get(key);
        }
        return msg;
    }

    /**
	 * Restituisce il codice del messaggio estraendolo dal nome del parametro
	 * usato nella configurazione.
	 */
    public String getMessageCode(String key) {
        String code = "999";
        String tok;
        if (params.containsKey(key)) {
            StringTokenizer st = new StringTokenizer(key, ".");
            while (st.hasMoreTokens() && code.equals("999")) {
                tok = st.nextToken();
                if ("0123456789".indexOf(tok.substring(0, 1)) >= 0) {
                    // il codice del msg e' il primo numero trovato nella
					// stringa key
                    code = tok;
                }
            }
        }
        return code;
    }
}

