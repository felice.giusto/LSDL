package com.eng.lsdl.core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

public class LSdl implements Runnable {

	private static final String className 		=	"Sdl";
	private static int maxConnections = 0;
	private ServerSocket listener = null; 
	
	private int thePort;
	private String theLinesBaseFile;
	private String theCacheName;
	private String theSourceIp;
	private String theMsisdnFilter;
	
	public  LSdl(int port,String linesBaseFile, String cacheName, String sourceIp, String msisdnFilter) {	
    	thePort = port;
    	theLinesBaseFile = linesBaseFile;
    	theCacheName = cacheName;
    	theSourceIp = sourceIp;
    	theMsisdnFilter = msisdnFilter;
	}
    	
	public void run() {
	   	String methodName = className + ".run";
    	Logger logger = Logger.getLogger(methodName);
    	
	    int i = 0;	
	    try {	    	
	    	InetAddress theAddress = InetAddress.getByName(theSourceIp);
	        listener = new ServerSocket(thePort, 50,theAddress);
//	    	listener = new ServerSocket(thePort);
	        
	        Socket server;
	
	        while ((i++ < maxConnections) || (maxConnections == 0)) {
	            server = listener.accept();
	            System.out.println("Connessione numero: ["+i+"]");
	            LSdlSocketServer conn_c = new LSdlSocketServer(server,theLinesBaseFile,theCacheName,theMsisdnFilter, (i==1) ? true : false);
	            Thread t = new Thread(conn_c);
	            t.start();
	        }
	    } catch (IOException ioe) {
	        logger.error("IOException on socket listen: " + ioe);
	        ioe.printStackTrace();
	    } finally {
	    	try {
				listener.close();
			} catch (IOException ioEx) { 
				logger.error("Failed to close listener: " + ioEx);
			}
	    }
	}
}