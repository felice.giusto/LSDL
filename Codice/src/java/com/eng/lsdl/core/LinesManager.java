package com.eng.lsdl.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import COM.stevesoft.pat.RegSyntax;
import COM.stevesoft.pat.Regex;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;

public class LinesManager {
	private static final String className 		=	"LinesManager";	
	private Map<String,char[]> theTmpLinesBase = null;
	private static LinesManager me = null;
	private File theLinesFile = null;
	private String theMsisdnFilter = null;
	
	private String theLinesPath				= "./SRV_LINEAS.DAT";
	private static NamedCache theLinesBase 	= 	null;
	private static String IS_RELOADING = "RELOADING";
	
    private LinesManager(String linesPath, String cacheName, String msisdnFilter) {    	
    	theLinesPath =	linesPath;	
    	theLinesBase =	CacheFactory.getCache(cacheName);
    	theLinesFile = new File(theLinesPath);
    	theMsisdnFilter = msisdnFilter;
    	
    	if (theLinesBase.isEmpty()) {    		
    		this.readLines();
    	} 
    }    
        
    public static LinesManager getLinesManager(String linePath, String cacheName, String theMsisdnFilter) {
        if (me == null) {
          synchronized (com.eng.lsdl.core.LinesManager.class) {
            if (me == null) {
              me = new com.eng.lsdl.core.LinesManager(linePath, cacheName, theMsisdnFilter);
            }
          }
        }
        return me;
    }
    

		
    public Character searchLine(String msisdn, String notUsingCache) {
    	String methodName = className + ".searchLine";
    	Logger logger = Logger.getLogger(methodName);    	
    	
    	//Regex rule = Regex.perlCode(theMsisdnFilter);
    	Regex rule = new Regex(new String(theMsisdnFilter));
    	
		rule.search(msisdn);
		
		if (!rule.didMatch()) {
			logger.debug("Line ["+msisdn+"] did not match the Format pattern.");
			return null;
		}
    	
		if (msisdn.startsWith("54")) { 
			msisdn = msisdn.substring(2);
		} else if (msisdn.startsWith("+54")) {
			msisdn = msisdn.substring(3);		
		} else if (msisdn.startsWith("0054")) {
			msisdn = msisdn.substring(4);		
		}
		
    	return notUsingCache.equalsIgnoreCase("M") ? searchLineMixed(msisdn) : (notUsingCache.equalsIgnoreCase("Y") ? searchLineFromFile(msisdn,false) : searchLineFromCache(msisdn));
    }
    
    private Character searchLineMixed(String msisdn) {
    	String methodName = className + ".searchLineMixed";
    	Logger logger = Logger.getLogger(methodName);
    	logger.info("Searching in Mixed mode: ["+theLinesPath+"] line ["+msisdn+"]");
    	
    	Character theViaFerraraMarket = null;
    	
    	logger.info("Searching in cache: line ["+msisdn+"]");
    	theViaFerraraMarket = searchLineFromCache(msisdn);
    	if (theViaFerraraMarket == null) { 
    		logger.info("Searching in file: line ["+msisdn+"]");
    		theViaFerraraMarket = searchLineFromFile(msisdn,true);
    		if (theViaFerraraMarket != null) {
    			logger.info("Adding to cache: line ["+msisdn+"]");
    			insertLine(msisdn, ""+theViaFerraraMarket);
    		}
    	}
    	
    	if (theViaFerraraMarket==null) logger.info("NOT FOUND: line ["+msisdn+"]");
    	return theViaFerraraMarket;
    }
    
    private Character searchLineFromCache(String msisdn) {
    	char[] theHashRangeLocal = (char[])theLinesBase.get(msisdn.substring(0,5));
    	
    	if (theHashRangeLocal==null) return null;
    	int indice = Integer.parseInt(msisdn.substring(5, msisdn.length()));
    	char theReturnedMarket = (theHashRangeLocal==null) ? null : theHashRangeLocal[indice]; 
 
    	return theReturnedMarket=='\u0000' ? null : (theReturnedMarket=='X' ? null : theReturnedMarket);    	
    }
    
    private Character searchLineFromFile(String msisdn, boolean reverse) {
    	String methodName = className + ".searchLineFromFile";
    	Logger logger = Logger.getLogger(methodName);
    	logger.debug("Searching in DAT File: ["+theLinesPath+"] line ["+msisdn+"]");
    	
    	Scanner scanner = null;
		try {
			scanner = new Scanner(theLinesFile);
		} catch (FileNotFoundException e) {
			logger.error("Failed to Scan Lines Base File ["+theLinesPath+"]");	    	 
		}
		    	
    	char theReturnedMarket = '\u0000';
    	
		if (reverse==false) {
			String lineInfo = null;
	    	while(scanner.hasNext()){
			 lineInfo = scanner.next().trim();
//			 logger.debug("LINE: ["+lineInfo+"]");
		     if(lineInfo.contains(msisdn)){
		    	logger.debug("<<<FOUND LINE>>>: ["+lineInfo+"]");
		        StringTokenizer market = new StringTokenizer(lineInfo,",");
		        market.nextElement();
		        theReturnedMarket = ((String)market.nextElement()).charAt(0);
//		        break;
		      }
			}
		    scanner.close();		    
		} else {		
			BufferedReader in = null;
			try {
				in = new BufferedReader (new InputStreamReader (new FileInputStream(theLinesFile)));
	
				String readingLineInfo = null;
				while(true) {
				    readingLineInfo = in.readLine();
				    if (readingLineInfo == null) break;
				    
					 logger.debug("LINE: ["+readingLineInfo+"]");
				     if(readingLineInfo.contains(msisdn)){
				    	logger.debug("<<<FOUND LINE>>>: ["+readingLineInfo+"]");
				        StringTokenizer market = new StringTokenizer(readingLineInfo,",");
				        market.nextElement();
				        theReturnedMarket = ((String)market.nextElement()).charAt(0);
				     }	
				}
			} catch (FileNotFoundException genEx) {
				logger.error("Failed Reading DAT file: ["+theLinesPath+"]"+genEx);
			} catch (IOException ioEx) {
				logger.error("IOException Reading DAT file: ["+theLinesPath+"]"+ioEx);
			}
		}
 
    	return theReturnedMarket=='\u0000' ? null : (theReturnedMarket=='X' ? null : theReturnedMarket);    	
    }
	
	public boolean insertLine(String theMsisdn,String theMarket) {
		if (setLinesMemory(theMsisdn+","+theMarket,false)==false) return false;
		return fileAppender(theMsisdn+","+theMarket);
	}
    
	public boolean deleteLine(String theMsisdn) {
		setLinesMemory(theMsisdn+",X",false);
		return fileAppender(theMsisdn+",X");
	}
	
	private boolean fileAppender(String theEntry) {
    	String methodName = className + ".fileAppender";
    	Logger logger = Logger.getLogger(methodName);
    	
		boolean theResult = false;
		try {
		   PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(theLinesPath, true)));
		   out.println(theEntry);
		   out.close();
		   theResult=true;
		}catch (IOException genEx) {
			logger.error("Failed Updating th File: ["+theLinesPath+"]"+genEx);
		}
		return theResult;
	}
	
	public synchronized boolean readLines() {
    	String methodName = className + ".readLines";
    	Logger logger = Logger.getLogger(methodName);
    	
		theTmpLinesBase = new Hashtable<String,char[]>();
		
		boolean theResult = false;		
		Scanner fileScanner = null;
		
		try {
			logger.debug("The Line Files: ["+theLinesFile+"]");
			fileScanner = new Scanner(theLinesFile);
			while (fileScanner.hasNext()){				
				String theLineSgring = fileScanner.next();
//				logger.debug("The Line: ["+theLineSgring+"]");
				setLinesMemory(theLineSgring, true);				
			}
			logger.debug("Done");
			theResult=true;
		} catch (Exception genEx) {
		       logger.error("Exception scannning Lines File: " + genEx);
		} finally {
			fileScanner.close();
		}
		
		logger.debug("Lines Base Read Completed");
		
		theLinesBase.clear();
		logger.debug("Chache Cleared");
		theLinesBase.putAll(theTmpLinesBase);
		logger.debug("Cache Reloaded");
		theTmpLinesBase.clear();
		theTmpLinesBase = null;
		logger.info("Done.");
		
		return theResult;
	}
	
	private boolean setLinesMemory(String theLineString, boolean isFullLoad) {
    	String methodName = className + ".setLinesMemory";
    	Logger logger = Logger.getLogger(methodName);
    	
    	boolean result = true;    	
    	
		char[] theLinesRangeHash = null;	
		
		String theLine		= null;
		String thePrefix	= null;
		String theMarket	= null;
		
		if (theLineString.startsWith("54")) { 
			theLineString = theLineString.substring(2);
		} else if (theLineString.startsWith("+54")) {
			theLineString = theLineString.substring(3);		
		} else if (theLineString.startsWith("0054")) {
			theLineString = theLineString.substring(4);		
		}
		
		thePrefix = theLineString.substring(0, 5);
				
		try {
			StringTokenizer theTokenizer = new StringTokenizer(theLineString.substring(5, theLineString.length()),",");
			theLine = theTokenizer.nextToken();			
			
			Regex rule = new Regex(new String(theMsisdnFilter));
	    	
			rule.search(thePrefix+theLine);				
			
			if (!rule.didMatch()) {
				logger.debug("Line ["+thePrefix+theLine+"] did not match the Format pattern.");
				return false;
			}
			
			theMarket = theTokenizer.nextToken();
			
			theLinesRangeHash = isFullLoad ? (char[])theTmpLinesBase.get(thePrefix) : (char[])theLinesBase.get(thePrefix);
			theLinesRangeHash = (theLinesRangeHash==null) ? (new char[100000]) : theLinesRangeHash;
						
			int indice = Integer.parseInt(theLine);
			theLinesRangeHash[indice]=theMarket.charAt(0);
			
			if (isFullLoad==false) theLinesBase.put(thePrefix, theLinesRangeHash);
			else theTmpLinesBase.put(thePrefix, theLinesRangeHash);			
		} catch (NoSuchElementException noSuchElEx) {
			logger.error("Malformed line Element. Failed to parse. Process Goes ahead.");
		}
		
		return result;
	}
	
	public boolean isReloading() {	
		//System.out.println("IS RELOADING BEGIN");
		String isReloading = (String)theLinesBase.get(IS_RELOADING);
		//System.out.println("IS RELOADING>>>>>>>>> ["+isReloading+"]");
		//System.out.println("IS RELOADING BEGIN");
		return isReloading.equalsIgnoreCase(IS_RELOADING);		
	}
	
	public void setIsReloading() {
		System.out.println("SETTTING RELOADING FLAG");
		theLinesBase.put(IS_RELOADING,IS_RELOADING);
		System.out.println("DONE ["+(String)theLinesBase.get(IS_RELOADING)+"]");
	}
	
	public void unsetIsReloading() {		
		theLinesBase.put(IS_RELOADING,"");
	}
}