package com.eng.lsdl.core;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.eng.lsdl.common.ConfigurationManager;
import com.eng.lsdl.common.Parameters;

class LSdlSocketServer implements Runnable {
    private Socket server;
    
    /*
     *	servicio: 01
     *	Formato: [servicio] [operacion] [linea],[mercado]
     *	operaciones:
	 *	I, insert
	 *	D, delete
	 *	S, Search
	 *
	 *	para el servicio 00: codigo de  retorno:
	 *  0, OK
	 *  1, El servidor de l�neas estar� ocupado por mucho tiempo, el mensaje se debe reenviar luego
	 *	2, Error de parseo de mensaje o alg�n otro error irrecuperable. Se debe descartar este mensaje
	 *
     */
    private static final String className 		=	"SdlSocketServer";
    private String theService	= null;
    private char theOperation;
    private String theLine		= null;
    private String theMarket	= null;
    private int theCRC			= -1;
    
    private ConfigurationManager configurator = null;
    
    private LinesManager linesManager = null;
    
    public static final char INSERT = 'I';
    public static final char DELETE = 'D';
    public static final char SEARCH = 'S';
    
    public static final char PING = 'P';
    public static final char ALTA = 'A';
    public static final char BAJA = 'B';
    
    private final String SERVICE_00 = "00";
    private final String SERVICE_01 = "01";
    private final String SERVICE_02 = "02";
    private final String RELOAD = "RELOAD";
    private final String REFRESH = "REFRESH";
    
    private final String RESPONSE_0 = "0\n\0";
    private final String RESPONSE_1 = "1\n\0";
    private final String RESPONSE_2 = "2\n\0";
    
    
    /**
	*   
	*	MQ_RET_OPERACION_OK		0   reciben �0000 nrolinea,mercado� (ej: �0000 3516519528,B�)
	*	Esta respuesta s�lo la reciben si la l�nea se encontr� en el servidor de l�neas
	*/
    private final String MQ_RET_OPERACION_OK = "0000\n\0";
    
	/**	MQ_RET_OPERACION_ERROR  1       � reciben �0001�
	*	Reciben esta respuesta cuando la l�nea no fue encontrada en el servidor de l�neas
	*/
    private final String MQ_RET_OPERACION_ERROR = "0001\n\0";
    
	/**	MQ_RET_ERROR_EJECUCION	2	reciben �0002�
	*	No encontr� que acci�n realizar con la operaci�n indicada (como que la operaci�n no est� definida)
	*/
	 private final String MQ_RET_ERROR_EJECUCION = "0002\n\0";
   
    
	/**	MQ_RET_ERROR_PARAMS		3	reciben �0003�
	*	Reciben esta respuesta cuando: el servicio no es v�lido 
	*	(Uds usan servicio �01� por lo que no deber�an recibir este c�digo de error), 
	*   la operaci�n no es valida
	*   (Uds utilizan operaci�n �S� (search) por lo que tampoco deber�a darse este c�digo de error) 
	*   o en general el mensaje recibido no tiene el formato establecido para el protocolo de comunicaci�n.
	*/
    private final String MQ_RET_ERROR_PARAMS = "0003\n\0";
    
	/**	 
	*	MQ_SERVER_BUSY_LONG_TIME 8	reciben �0008�
	*	El servidor de l�neas est� ocupado y no est� recibiendo peticiones (esta situaci�n ocurre durante el refresh).
	**/
    private final String MQ_SERVER_BUSY_LONG_TIME = "0008\n\0";
    
    
    
    LSdlSocketServer(Socket theServer, String theLinesBaseFile, String theCacheName, String theMsisdnFilter, boolean reset) {
        configurator = ConfigurationManager.getConfigurator();
    	this.server = theServer;	        
        linesManager = LinesManager.getLinesManager(theLinesBaseFile,theCacheName,theMsisdnFilter);
        if (reset==true) {
        	System.out.println("First Connection. Resetting Reload Flag.");
        	linesManager.unsetIsReloading();
        }
    }    

    public void run() {
    	String methodName = className + ".run";
    	Logger logger = Logger.getLogger(methodName);
    	logger.debug("Begin");
    	
    	String theRequest = null;
    	String theResponse = null;    	
    	BufferedReader inFromClient = null; 
    	DataOutputStream outToClient  = null;
    	
        try {
            // Get input from the client
            inFromClient = new BufferedReader(new InputStreamReader(server.getInputStream()));
            outToClient = new DataOutputStream(server.getOutputStream());

          while(true) {        	   
           	char[] inBuf = {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'};
            inFromClient.read(inBuf,0,23); 

            if (inBuf[0]==-1 || inBuf==null || inBuf.length==0 || inBuf[0]=='\0' || inBuf[0]=='\r' || inBuf[0]=='\n' || (inBuf[0]=='\r' && inBuf[1]=='\n')) break;        
            
            
            theRequest = new String(inBuf);
            logger.debug("GOT: "+theRequest);
            
            theResponse = decodeCommand(theRequest.trim());
            outToClient.write(theResponse.getBytes());       
            outToClient.flush();
            logger.debug("Write OK");
           }
        } catch (IOException ioe) {
            logger.error("Exception listening on socket: " + ioe);
        } catch (Exception genEx) {
            logger.error("Exception: " + genEx);
        } finally {
            try {
				server.close();				
				logger.debug("Server Closed");
				outToClient.close();
				logger.debug("OutPutStream Closed");
			} catch (IOException ioex) {
				logger.error("Failed to Close: " + ioex);
			}

        }
        logger.debug("End");
    }    
    
    private String decodeCommand(String receivedCommand) {
    	String methodName = className + ".decodeCommand";
    	Logger logger = Logger.getLogger(methodName);
    	
    	String theResponse = null;
    	theService		= null;
    	theOperation	= '\u0000';
    	theLine			= null;
    	theMarket		= null;
    	theCRC			=-1;
    	
    	try {
    		System.out.println("Reloading ["+linesManager.isReloading()+"]");
    		if (linesManager.isReloading()) {
        		theResponse = MQ_SERVER_BUSY_LONG_TIME;
        		logger.info("System is reloading. Rejected command:"+receivedCommand);	
    		} else if (receivedCommand.equalsIgnoreCase(REFRESH)) {
    			configurator=ConfigurationManager.refreshConfigurator();
    			if (configurator!=null)
    				theResponse=MQ_RET_OPERACION_OK;
    			else
    				theResponse=MQ_RET_ERROR_PARAMS;
    		} else if (receivedCommand.equalsIgnoreCase(RELOAD)) {
    			linesManager.setIsReloading();
    			System.out.println("Has set Reloading ["+linesManager.isReloading()+"]");
    			if (linesManager.readLines()==true)
    				theResponse=MQ_RET_OPERACION_OK;
    			else
    				theResponse=MQ_RET_ERROR_PARAMS;
    			linesManager.unsetIsReloading();
    		} else {	    		
		        StringTokenizer theRequest = new StringTokenizer(receivedCommand," ");	        		        		        	
		        	try {
		            	theService		= theRequest.nextToken().trim();		            				        	
				        	
		            	int intService;
		            	try {
		            		intService = Integer.parseInt(theService);
		            	} catch (Exception ex) {
			        		logger.error("Service value non numeric: ["+theService+"]");
			        		return MQ_RET_ERROR_PARAMS;
		            	}
		            	
		            	switch (intService)	{
		            		case 0:
		            		case 2:
		            			theLine			= theRequest.nextToken().trim();
		            			theMarket		= theRequest.nextToken().trim();
		            			theOperation	= theRequest.nextToken().trim().charAt(0);				            	
//		            			theCRC			= Integer.parseInt(theRequest.nextToken().trim());
		            			break;
		            		case 1:
		            			theOperation	= theRequest.nextToken().trim().charAt(0);
				            	theLine			= theRequest.nextToken(",").trim();
				            	theMarket		= theRequest.nextToken().trim();
				            	break;		
		            		default:
				        		logger.error("Service value not recognized: ["+theService+"]");
				        		return MQ_RET_ERROR_PARAMS;
		            	}
		            			            			        				            	
		            	if (theOperation==SEARCH) {
		        			Character gotMarket = linesManager.searchLine(theLine, configurator.getParameterValue(Parameters.PARAM_LSDL_NO_CACHE));			        			
		        			if (gotMarket==null) theResponse= (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_2 : MQ_RET_OPERACION_ERROR;
		        			else theResponse = "0000 "+theLine+","+gotMarket+"\n\0";
		        		} else if(theOperation==INSERT || theOperation==ALTA) {			        			
		        			boolean isInserted = linesManager.insertLine(theLine,theMarket);
		        			if (isInserted==true) theResponse= (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_0 : MQ_RET_OPERACION_OK;
		        			else theResponse = (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_2 : MQ_RET_OPERACION_ERROR;
		        		} else if (theOperation==DELETE || theOperation==BAJA) {
		        			boolean isDeleted = linesManager.deleteLine(theLine);
		        			if (isDeleted==true) theResponse= (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_0 : MQ_RET_OPERACION_OK;
		        			else theResponse= (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_2 : MQ_RET_OPERACION_ERROR;
		        		} else if (theOperation==PING) {
		        			theResponse=RESPONSE_0;
		        		} else theResponse= (theService.equalsIgnoreCase(SERVICE_00)) ? RESPONSE_2 : MQ_RET_ERROR_EJECUCION;

		        	} catch (NoSuchElementException noSElemEx) {
		        		logger.error("Malformed Request. ["+receivedCommand+"]");
		        		theResponse=MQ_RET_ERROR_PARAMS;
		        	}
		        	
		        	logger.debug("<"+theService+"> <"+theOperation+"> <"+theLine+"> <"+theMarket+">");		        			        			       
    		}    		    	
	    } catch (Exception genEx) {
	    	logger.error("Failed decoding Command: [" + receivedCommand+"]");
	    	logger.error("Exception:" + genEx);
	    	theResponse=MQ_RET_ERROR_PARAMS;
	    	linesManager.unsetIsReloading();
	    }
	    return theResponse;
    }
}
