package com.eng.lsdl.core;


import java.io.Serializable;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import weblogic.common.T3ServicesDef;

import com.eng.lsdl.common.ConfigurationManager;
import com.eng.lsdl.common.Parameters;

public class LSDLService implements Serializable  {
//public class LSDLService implements T3StartupDef, Serializable  {	  		
	private static final String className 		=	"LSDLService";
	private static final long serialVersionUID	=	-15285057742931719L;	
	private static LSdl theSdl					=	null;
	private ConfigurationManager configurator;    

//	public String startup(String name, Hashtable params) throws Exception {
//		return go();
//	}
	
	public static void main(String[] args) {
		String methodName = className + ".main";
		Logger logger = Logger.getLogger(methodName);
		
		LSDLService lsdlS = new LSDLService();
		try {
			lsdlS.go();
		} catch (Exception e) {
			logger.info("LSDL is Activated");
		}
	}
	
	public String go() throws Exception {
		configurator = ConfigurationManager.getConfigurator();			
		
		String methodName = className + ".startup";
		Logger logger = Logger.getLogger(methodName);
		
		logger.info("Activating LSDL Service");
				
		int thePort 			= Integer.parseInt(configurator.getParameterValue(Parameters.PARAM_LSDL_PORT));
		String theCacheName 	= configurator.getParameterValue(Parameters.PARAM_LSDL_CACHE_NAME);
		String theLinesBaseFile = configurator.getParameterValue(Parameters.PARAM_LSDL_LINES_FILE);
		String theListenIp 		= configurator.getParameterValue(Parameters.PARAM_LSDL_LISTEN_IP);
		String theMsisdnFilter	= configurator.getParameterValue(Parameters.PARAM_LSDL_MSISDN_FILTER);
		
		logger.debug("LSDL Port: ["+thePort+"]");
		
		new Thread(new LSdl(thePort, theLinesBaseFile,theCacheName, theListenIp,theMsisdnFilter)).start();

		logger.info("LSDL is Activated");
		return "Sdl Socket Server Started";
	}		
	
	public void setServices(T3ServicesDef services) {
	}			
}