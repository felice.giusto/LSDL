*******************************************************************************
LSDL readme

Procedura operativa per il build del progetto.
*******************************************************************************

PREREQUISITI

	- 	Istallazione della versione jdk160_29 del jre (Java Run-Time Enviroment). 
		La variabile di ambiente 		
			JAVA_HOME
		deve puntare a questa istallazione.


	-	BEA 10.3.6
		* Impostare la variabile d'ambiente BEA_HOME
		 (installazione di BEA: es. C:\bea)

		* Impostare dal prompt della finestra DOS di compilazione la variabile
			 WEBLOGIC_HOME

		  in modo che punti alla directory contenente il progetto:
			set WEBLOGIC_HOME=<Workset Root Directory>\src\java

	          es:
			set WEBLOGIC_HOME=D:\lsdl\src\java			

	-	ANT 1.7.0 
		* Impostare la variabile d'ambiente ANT_HOME
		  (installazione	di ANT: es. C:\ant)
		* Impostare ant nel path es set PATH=%ANT_HOME%\bin;%PATH%;


	- In aggiunta ai file del progetto, assicurarsi della presenza dei seguenti item:
		* item "/src/build.cmd"
		* item "/src/build.xml"


******************************************************************************
BUILD LSDL
******************************************************************************

      Come prerequyisito, verificare la presenza in: 
	  \src\java\lib


      A questo punto � possibile eseguire la build:
		> build lsdl [INVIO]

	- Se la build termina con successo, il file disponibile nella directoy:	
		* src/build/lib

	sar�:		
		* lsdl-lib.jar

Il file ottenuto dovr� poi essere riportato nel ws nella directory:
	 delivery\lib\
******************************************************************************

