@echo off

cls

title LSDL Build Task

setlocal

set JAVA_HOME=D:\bea\jdk160_29

set WEBLOGIC_HOME="D:\LSDL"\src\java\

echo %WEBLOGIC_HOME%

rem set BEA_HOME=C:\bea

set PATH=%JAVA_HOME%\bin;%PATH%

set CLASSPATH=.;%ANT_HOME%\lib\*;

echo %JAVA_HOME%

%JAVA_HOME%\bin\java -Xmx512M -Dwl_version=%WL_VERSION% -Dcom.sun.xml.namespace.QName.useCompatibleSerialVersionUID=1.0 -Dweblogic.home=%WEBLOGIC_HOME% -classpath "%ANT_HOME%\lib\ant-launcher.jar" org.apache.tools.ant.launch.Launcher -f build.xml %1

echo.

echo.

pause

endlocal